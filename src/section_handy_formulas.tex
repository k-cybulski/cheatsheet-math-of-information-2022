\section{Handy formulas}\label{sec:handy}

\begin{sectionbox}[Matrices]
    \begin{enumeratenosep}[i.]
        \item $\text{tr} \left( UU^{H} \right)  = \sum_{i = 1}^{m} \sum_{j = 1}^{m} \left| U_{ij} \right|^{2}$ for $U \in \mathbb{C} ^{m \times m}$.
        \item The $\left\Vert \cdot \right\Vert _2$ norm is rotationally invariant, so for unitary matrices $\left\Vert Ux \right\Vert _2 = \left\Vert x \right\Vert _2$.
        \item Sylvester's inequality: Let $A \in \mathbb{C} ^{m \times n}$ and $B \in \mathbb{C} ^{n \times k}$ be matrices.    $ \text{rank} \left( A \right)  + \text{rank} \left( B \right)  - n \le \text{rank} \left( AB \right) \le \min \left\{ \text{rank} \left( A \right) , \text{rank} \left( B \right)  \right\}$
    \end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[Fourier transforms]
    \begin{enumeratenosep}[i.]
    \item     $x\left( at \right) \stackrel{\mathcal{F}  }{\longmapsto} \frac{1}{\left| a \right|}\hat{x}\left( f/a \right).$
    \item $x\left( t - t_0 \right) \cdot \stackrel{\mathcal{F} }{\longmapsto} \hat{x}\left( f \right) e^{-i2\pi ft_0} $
    \item $x\left( at - t_0 \right) \stackrel{\mathcal{F} }{\longmapsto} \frac{1}{\left| a \right|} \hat{x}\left( f/a \right)e^{-i2\pi \left( f/a \right)t_0}$
    \item $x\left( t \right) \cdot e^{-i2\pi f_0t} \stackrel{\mathcal{F} }{\longmapsto} \hat{x}\left( f + f_0 \right).$
    \item $    \sum_{k = -\infty}^{\infty} x(t + kT) = \frac{1}{T} \sum_{k = -\infty}^{\infty} \hat{x} \left( k/T \right)e^{i2\pi f \frac{t}{T}}$
    \item $\delta \left( t \right) \stackrel{\mathcal{F} }{\longmapsto} 1$.
    \item $c_0\delta \left( t - t_0 \right) \stackrel{\mathcal{F} }{\longmapsto} c_0e^{-i2\pi ft_0}$
    \item         The Fourier transform maps convolutions to products, so if $y\left( t \right) = \left( x * h \right)\left( t \right)$ then $    \hat{y}\left( f \right) = \hat{x}\left( f \right) \hat{h}\left( f \right).$
    \item The Fourier transform of a Fourier transform of $x(t)$ is $x(-t)$: $\hat{x}\left( f \right) \stackrel{\mathcal{F} }{\longmapsto} x\left( -t \right)$.
    \item $e^{ix} = \cos x + i\sin x$
    \end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[Inequalities]
    \begin{enumeratenosep}[label=\roman*]
    \item Triangle: $\left\Vert x + y \right\Vert  \le \left\Vert x \right\Vert  + \left\Vert y \right\Vert $.
    \item Reverse triangle: $\left| a + b \right| \ge  \left| a \right| - \left| b \right|$
    \item Cauchy-Schwarz: For a Hilbert space $\mathcal{H} $, $\left| \left\langle x, y \right\rangle_{\mathcal{H} }  \right|\le \left\Vert x \right\Vert_{\mathcal{H} } \left\Vert y \right\Vert_{\mathcal{H} } $
    \item Jensen: If $\phi $ is convex, then $\phi \left( \Exnb \left[ X \right] \right) \le \Exnb \left[ \phi \left( X \right) \right] $.
    \end{enumeratenosep}
\end{sectionbox}


\begin{sectionbox}[Coherence]
    \begin{enumeratenosep}[i.]
    \item If $A, B$ are unitary then $\mu \left( \begin{bmatrix}
        A & B
\end{bmatrix} \right) = \mu \left( \begin{bmatrix}
    I & A^{H}B
\end{bmatrix} \right)$
\item   We can write the maximum element of a matrix $U$ as             $    \max _{k, l} \left| U_{k, l} \right|^{2} = \mu ^{2} \left( \begin{bmatrix}
        I & U
    \end{bmatrix} \right)$.
    \end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[Shuffling inner products and norms]
    \begin{enumeratenosep}[i.]
        \item $K \in \mathbb{C} , x \in \mathcal{H}, y \in \mathcal{H}  $: $\left\langle x, Ky \right\rangle = \left\langle \overline{K}x, y \right\rangle = \overline{K}\left\langle x, y \right\rangle $.
        \item $\left| \left\langle x, y \right\rangle  \right|^{2} = \left\langle x, y \right\rangle \overline{\left\langle x, y \right\rangle }$.
        \item If $\left\vvvert \cdot \right\vvvert $ is an operator norm ($\left\vvvert A \right\vvvert = \sup _{\left\Vert x \right\Vert  = 1} \left\Vert Ax \right\Vert $) then $\left\Vert ABx \right\Vert \le \left\vvvert A \right\vvvert \left\Vert Bx \right\Vert$ 
        \item $v \in \text{ker} \left( A \right)$, $u \in \text{im} \left( A^{H} \right)$ implies $\left\langle v, u \right\rangle  = 0$.
    \end{enumeratenosep}
\end{sectionbox}


\begin{sectionbox}[$z$-transforms]
    Defined in section \nameref{sec:fri_esprit}.
    \begin{enumeratenosep}[i.]
        \item     If the function $x\left[ n \right]$ has $z$-transform $\hat{x}\left( z \right)$, then the shifted function $x'\left[ n \right] = x\left[ n - 1 \right]$ has transform $\hat{x}'\left( z \right) = \hat{x}\left( z \right)z^{-1}$.
        \item Sifting property $a\left[ n \right] * \delta \left[ n - k \right] = a\left[ n - k \right]$
    \end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[Series]
    \begin{enumeratenosep}[label=\roman*]
        \item Geometric sum: $\sum_{k = 0}^{n} ar^{k} = a\left( \frac{1 - r^{n + 1}}{1 - r} \right)$.
        \item Taylor: $\sum_{n = 0}^{\infty} \frac{f^{(n)}\left( a \right) \left( x - a \right)^{n}}{n!}$
        \item Exponential: $\exp \left[ x \right] = \sum_{n = 0}^{\infty} \frac{x^{n}}{n!}$
    \end{enumeratenosep}
\end{sectionbox}

