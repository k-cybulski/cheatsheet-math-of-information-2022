\section{Uncertainty relations and signal recovery}\label{sec:uncertainty}

\begin{sectionbox}[Notation]
    \begin{enumeratenosep}[i.]
  \item   $ \left( D_{\mathcal{A} } \right)_{i,i} =    \begin{cases}
               1    &\text{ if } i \in \mathcal{A} ,\\
               0 &\text{ if } i \notin \mathcal{A} .
           \end{cases}$
       \item $   P_\mathcal{A} \left( U \right) = UD_{\mathcal{A} }U^{H}$.
        
    \item    $\mathcal{W} ^{U, \mathcal{A} }$ is the image space of  $P_\mathcal{A} \left( U \right)$, i.e. $\mathcal{W} ^{U, \mathcal{A} } = \mathcal{R} \left( P_{\mathcal{A} }\left( U \right) \right)$
  \item              \[\begin{aligned}
                   \Delta _{P, Q}\left( U \right) &= \left\vvvert D_P P_Q \left( U \right) \right\vvvert _2 \\
                                                  &=\max _{x \in \mathcal{W} ^{U, Q} \setminus \left\{ 0 \right\}} \frac{\left\Vert x_P \right\Vert _2}{\left\Vert x \right\Vert _2} &\text{(Lemma 2.20)} 
               \end{aligned}\]
    \end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[]
    For a matrix $\textbf{A} $ it is true that
\[
    \frac{\left\Vert \textbf{A} \right\Vert _2}{\sqrt{\text{rank} \left( \textbf{A}  \right)} } \le \left\vvvert \textbf{A}  \right\vvvert _2 \le \left\Vert \textbf{A}  \right\Vert _2.
\] Notably, if we take $\textbf{A} = \Delta _{\mathcal{P} , \mathcal{Q} }\left( U \right)$ we get an uncertainty relation.

                  \[
                                                \sqrt{\frac{\text{tr} \left( D_\mathcal{P} P_\mathcal{Q} \left( F \right) \right) }{\min \left( \left| \mathcal{P}  \right|, \left| \mathcal{Q}  \right| \right)}}  \le \Delta _{\mathcal{P} , \mathcal{Q} }\left( F \right) \le \sqrt{\text{tr} \left( D_\mathcal{P} P_\mathcal{Q} \left( F \right) \right) } .
                                            \] 

                                            For a DFT matrix,  $\text{tr} \left( D_\mathcal{P} P_\mathcal{Q} \left( F \right) \right)  = \frac{\left| \mathcal{P}  \right|\left| \mathcal{Q}  \right|}{m}$ so we can write
                                                                                            \[
                                                \sqrt{\frac{\max \left( \left| \mathcal{P}  \right|, \left| \mathcal{Q}  \right| \right)}{m}}  \le \Delta _{\mathcal{P} , \mathcal{Q} }\left( F \right) \le \sqrt{\frac{\left| \mathcal{P}  \right|\left| \mathcal{Q}  \right|}{m}} .
                                            \]

\end{sectionbox}

\begin{sectionbox}[Coherence]
            Suppose $A$ has columns $\left( a_1, \dots, a_n  \right)$ $\left\Vert \cdot \right\Vert _2$-normalized to 1. Then coherence is defined as       $           \mu \left( A \right) = \max _{i \neq j} \left| a_i^{H}a_j \right| $.

            If $A$ is orthogonal, then coherence is 0.


            We can use coherence to upper bound $\Delta _{\mathcal{P} , \mathcal{Q} }\left( U \right) \le \sqrt{\left| \mathcal{P}  \right|\left| \mathcal{Q}  \right|}  \mu \left( \left[ I \text{ } U \right] \right).$ (Proof \ref{proof:coherence_uncertainty_relation})

            Coherence is connected to the spark (See section \nameref{subsec:signal_recovery_with_undersampling}) by $\text{spark} \left( D \right) > 1 + \frac{1}{\mu \left( D \right)}.$ (Proof \ref{proof:bound_spark_by_coherence})

            For a matrix $D \in \Co^{m \times n}$ with $m \le n$ it is true that $\mu (D) \ge \sqrt{\frac{n - m}{m(n - 1)}} .$ (Proof \ref{proof:coherence_lower_bound_by_dimensions})
\end{sectionbox}

\begin{sectionbox}[Coherence of I and DFT matrix]
    The identity and DFT matrices are the most incoherent (with least coherence) pair of unitary matrices with $   \mu \left( \begin{bmatrix}
            I &F
        \end{bmatrix} \right) = \frac{1}{\sqrt{m} }.$
\end{sectionbox}

\subsection{$\varepsilon $-concentrated signals}
\begin{sectionbox}[Concentrated vectors]
    Let $\mathcal{P}  \subseteq \left\{ 1, \dots, m \right\}$ and $\varepsilon_P \in \left( 0, 1 \right)$. Then the vector $x \in \Co^{m}$ is said to be $\varepsilon_P$-concentrated if
\[
    \frac{\left\Vert x - x_\mathcal{P}  \right\Vert_2 }{\left\Vert x \right\Vert _2} \le \varepsilon_\mathcal{P} 
\] where $x_\mathcal{P}  = D_\mathcal{P} x.$ This means that the fraction of the $\left\Vert \cdot \right\Vert _2$ norm that resides outside of the indices $\mathcal{P} $ may be at most $\varepsilon _\mathcal{P} $.
\end{sectionbox}

\begin{sectionbox}[Uncertainty relations with concentrated vectors]
    Let $U \in \Co^{m \times m}$ be unitary and $\mathcal{P}, \mathcal{Q} \subseteq \left\{ 1, \dots, m \right\}$. Suppose that there exists a nonzero $\varepsilon _\mathcal{P}$-concentrated $p \in \Co^{m}$, and a nonzero $\varepsilon _\mathcal{Q}$-concentrated $q \in \Co^{m}$ such that $p = Uq$. It is true that     $\Delta_{\mathcal{P}, \mathcal{Q}} \left( U \right) \ge  \left[ 1 - \varepsilon _\mathcal{P} - \varepsilon _\mathcal{Q} \right]_+ $
\end{sectionbox}

\begin{sectionbox}[Elad-Bruckstein uncertainty relation]
     Let $A,B \in \Co^{m \times m}$ be unitary and $\mathcal{P}, \mathcal{Q} \subseteq \left\{ 1, \dots, m \right\}$. Suppose that there exists a nonzero $\varepsilon _\mathcal{P}$-concentrated $p \in \Co^{m}$, and a nonzero $\varepsilon _\mathcal{Q}$-concentrated $q \in \Co^{m}$ such that $Ap = Bq$. Then\[
    \left| \mathcal{P}  \right| \left| \mathcal{Q}  \right| \ge  \frac{\left[ 1 - \varepsilon _\mathcal{P}  - \varepsilon _\mathcal{Q}  \right]_+^{2}}{\mu ^{2}\left( \begin{bmatrix}
        A & B
    \end{bmatrix} \right)}.
\]

\end{sectionbox}


\begin{sectionbox}[Guaranteed linear noisy signal recovery]
    Let $U \in \Co^{m \times m}$ be unitary, $\mathcal{Q} \subset \left\{ 1, \dots, m \right\}$, $p \in \W^{U, \mathcal{Q}}$, and consider
\[
    y = p_{\mathcal{P}^{c}} + n,
\] where $n \in \Co^{m}$ and $\mathcal{P}^{c} = \left\{ 1, \dots, m \right\} \setminus \mathcal{P}$ . 
If $\Delta _{\mathcal{P} , \mathcal{Q} }\left( U \right) <1,$ which itself is guaranteed by \[
    \left| \mathcal{P}  \right| \left| \mathcal{Q}  \right| < \frac{1}{\mu ^{2}\left( \begin{bmatrix}
        I &U
    \end{bmatrix} \right)},\] then there exists a reconstruction matrix $L \in \mathbb{C} ^{m \times m}$ such that
\[
    \left\Vert L q  - p \right\Vert _2 \le C\left\Vert n_{\mathcal{P} ^c} \right\Vert _2
\] for a constant $C = 1/\left( 1 - \Delta _{\mathcal{P} , \mathcal{Q} }\left( U \right) \right).$ The term $C\left\Vert n_{\mathcal{P} ^{C}} \right\Vert $ is an upper bound on the reconstruction error.

The reconstruction matrix is given by $ L = \left( I - D_\mathcal{P} P_\mathcal{Q} \left( U \right) \right)^{-1}D_{\mathcal{P} ^{C}}$.

\end{sectionbox}

\subsection{Signal recovery with undersampling \& matrices}\label{subsec:signal_recovery_with_undersampling} 

\begin{notebox}
    The DFT matrices are defined in section \nameref{sec:fourier}.
\end{notebox}

% SKIP: Flashcard on s vs 2s-sparse sampling justification with DFT

\begin{sectionbox}[Vandermonde matrix]
    A Vandermonde matrix $V$ is of form
            \[\begin{aligned}
                      V &= \begin{pmatrix}
                          1 & 1 & \cdots & 1 \\\\
                          z_1 & z_2 & \cdots & z_L \\
                          \vdots & \vdots & \ddots &\vdots \\
                          z_1^{L - 1} & z_2^{L - 1} & \cdots & z_{L}^{L - 1}
                      \end{pmatrix}
                  \end{aligned}\] here $z_1, \dots, z_L$ are "nodes" of $V$.
\end{sectionbox}
\begin{sectionbox}
                  They are useful for signal processing as analysis/synthesis matrices, because if we remove $s$ columns and $s$ rows from a square Vandermonde matrix resulting in a square matrix $D$, we know that $D$ will still be invertible, allowing unique recovery.

                  Their determinant is $\text{Det} \left( V \right) = \prod_{1\le i<j\le L}^{} \left( x_j - x_i \right).$
\end{sectionbox}

\begin{sectionbox}[Spark of a matrix]
            The spark of a matrix $A$, denoted by $\text{spark} \left( A \right)$, is defined as the cardinality of the smallest set of linearly dependent columns of $A$.

            A vector $x$ which satisfies $Dx = 0$ ($x$ is in null space of $D$) must have at least $\text{spark} \left( D \right) $ nonzero elements $\left\Vert x \right\Vert _0 \ge \text{spark} \left( D \right) $.

            The spark of a matrix $D$ satisfies $\text{spark} \left( D \right) > 1 + \frac{1}{\mu \left( D \right)}.$ (Proof \ref{proof:bound_spark_by_coherence})
\end{sectionbox}

\begin{sectionbox}[General conditions for (P0) or (P1) recovery]
    The algorithms are correct if for all $\alpha \in \No\left( D \right)$, $\alpha  \neq 0$ we get that the correct solution $x$ has the least $\left\Vert \cdot \right\Vert $ norm out of all consistent solutions $\hat{x} = x + \alpha $ in their respective norms. So, for these algorithms
\begin{enumerate}[start=0, label=(P\arabic*)]
    \item $\left\Vert x + \alpha  \right\Vert _0 > \left\Vert x \right\Vert _0$,
    \item $\left\Vert x + \alpha  \right\Vert _1 > \left\Vert x \right\Vert _1$.
\end{enumerate}

See boxes below for more specific recovery conditions.
\end{sectionbox}


\begin{sectionbox}[Specific conditions for (P0) recovery]
            We can uniquely recover $x$ from $y = Dx$ if $s < \frac{\text{spark} \left( D \right) }{2}.$ The this implies that any set of $2s$ columns of $D$ will be linearly independent, $y_1, y_2$ will be unique for any two $s$-sparse vectors $x_1, x_2$.

        An alternative condition for unique (P0) recovery comes from the bound on spark by coherence. (P0) applied to $y = Dx$ recovers the $s$-sparse $x$ if $           \left\Vert x \right\Vert _0 \le s < \frac{1}{2}\left( 1 + \frac{1}{\mu \left( D \right)} \right).$
\end{sectionbox}

% skip consistency definition

\begin{sectionbox}[Recovery with (P0)]
    In the recovery problem attempting to find $x$ from $y = Dx$ for an $s$-sparse $x$ and a wide matrix $D$ with $\text{spark} \left( D \right)  > 2s$, we can use (P0).             The (P0) recovery algorithm searches through all possible consistent $\hat{x}$, and returns the one solution which has at most $s$ nonzero entries. Formally, it computes
        \[
            \arg \min \left\Vert \hat{x} \right\Vert _0 \text{ subject to } y = D\hat{x}.
        \]

        There is precisely one such solution, because $\hat{x}$ will have sparsity of at least $s + 1$ if $\hat{x}\neq x$, and otherwise it will have at most $s$ sparsity.       This is due to the fact we can write any consistent vector $\hat{x}$ as  $    \hat{x} = x + x'$ where $x$ is the correct solution, and we know that all elements of $x' \in \mathcal{N} \left( D \right)$ have sparsity of at least $\text{spark} \left( D \right)  \ge  2s + 1$. So, in the worst case scenario that $x'$ annihilates all entries of $x$, the sparsity $\left\Vert \hat{x} \right\Vert _0$ will be at least $s + 1$.

\end{sectionbox}


\begin{sectionbox}[Specific conditions for (P1) recovery]
    (P1) is guaranteed to succeed in recovering $x$ if $x$ has support set $S$ and $            P_1\left( S, D \right) < \frac{1}{2}$ where $   P_1\left( S, D \right) := \max _{x \in \No\left( D \right), x \neq 0 } \frac{\sum_{k \in S}^{} \left| x_k \right|}{\sum_{k}^{} \left| x_k \right|}.$ (Proof \ref{proof:guaranteed_recovery_with_p1})
\end{sectionbox}

