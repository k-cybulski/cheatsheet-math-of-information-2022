\section{Hilbert Spaces}\label{sec:hilbert_spaces}

\begin{sectionbox}[Vector space]
    A set $\X$ together with two operations $(+, \cdot)$ is a vector space over $\F$ if the following properties are satisfied:
\begin{enumerate}[(i)]
    \item The addition operation denoted by $+$ satisfies
    \begin{itemize}
        \item associativity, so $\left( x + y \right) + z = x + \left( y + z \right)$ for all $x, y, z \in \X$,
        \item commutativity, so $x + y = y + x$ for all $x, y \in \X$.
    \end{itemize}
    \item There exists an element $0 \in \X$, called the zero vector, such that $x + 0 = x$ for all $x \in \X$.
    \item For every $x \in \X$, there exists $-x \in \X$ such that $x + \left( -x \right) = 0$.
    \item The multiplication operator denoted by $\cdot$ satisfies
            \begin{itemize}
                \item $1 \cdot x = x$ for all $x \in \X$,
                \item $\alpha \cdot \left( \beta \cdot x \right) = \left( \alpha \beta  \right)\cdot x$ for all $\alpha,\beta  \in \F$ and $x \in \X$,
                \item $\left( \alpha +\beta  \right)\cdot x = \alpha \cdot x + \beta \cdot x$ for all $\alpha,\beta  \in \F$ and $x \in \X$,
                \item $\alpha \cdot \left( x + y \right) = \alpha \cdot x + \alpha \cdot y$ for all $\alpha  \in \F$ and $x, y \in \X$.
    \end{itemize}
\end{enumerate}

\end{sectionbox}

\begin{sectionbox}[Subspace]
    $\Y$ is a subspace of $\X$ if $\Y$ is itself a vector space and a subset of $\X$.

    An easy way to check if $\mathcal{Y} $ is a subspace is   to see if it contains 0 and if it is stable under linear combinations, i.e.$    \alpha \cdot y + \beta \cdot z \in \Y$ for all $\alpha , \beta  \in \F$ and $y, z \in \Y$.

\end{sectionbox}



\begin{sectionbox}[Norm]
A norm $\left\Vert \cdot \right\Vert $ on $\X$ is a function which maps $\X$ to $\R$ and satisfies
\begin{enumeratenosep}[label=\roman*]
    \item for all $x \in \X$, $\left\Vert x \right\Vert \ge 0$ and $\left\Vert x \right\Vert = 0$ implies $x = 0$ (positivity),
    \item for all $x \in \X$ and for all $\alpha \in \F$, $\left\Vert \alpha x \right\Vert = \left| \alpha  \right| \left\Vert x \right\Vert $ (homogeneity),
    \item for all $x, y \in \X$, $\left\Vert x + y \right\Vert  \le  \left\Vert x \right\Vert  + \left\Vert y \right\Vert$ (triangle inequality).
\end{enumeratenosep}
\end{sectionbox}
\begin{sectionbox}[Inner product]
An inner product $\left\langle \cdot, \cdot \right\rangle $ over $\F$ is a function $\X \times \X \to \F$ such that for all $x, y \in \X$ and $\alpha 
\in \F$, we have
\begin{enumeratenosep}[label=\roman*]
    \item symmetry, so $\left\langle y, x \right\rangle = \overline{\left\langle x, y \right\rangle }$,
    \item bilinearity, so $\left\langle \alpha x, y \right\rangle  = \alpha \left\langle x, y \right\rangle $ and $\left\langle x + y, z \right\rangle = \left\langle x, z \right\rangle + \left\langle y, z \right\rangle $,
    \item positivity, so $\left\langle x, x \right\rangle  \in \R$, $\left\langle x, x \right\rangle  \ge 0$, and $\left\langle x, x \right\rangle  = 0 \implies x = 0$.
\end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[Metric]
     A metric on $\mathcal{X} $ is a function $\rho : \mathcal{X}  \times \mathcal{X} \to \mathbb{R} $ such that
 \begin{enumeratenosep}[label=\roman*]
     \item $\rho \left( x, x' \right) \ge 0$ for all $x, x' \in \mathcal{X} $ with equality if and only if $x = x'$.
     \item $\rho \left( x, x' \right) = \rho \left( x', x \right)$.
     \item $\rho \left( x, \tilde{x} \right) \le \rho \left( x, x' \right) + \rho \left( x', \tilde{x} \right)$ for all $x, x', \tilde{x}$.
 \end{enumeratenosep}
\end{sectionbox}


\subsection{Norms on various spaces}

\begin{sectionbox}[Norms and inner products on vectors/functions in $\mathbb{C} $] 
        For vectors we have the sum inner product (with conjugate) and the squared $l^{2}$-norm
            $\left\langle \textbf{a} , \textbf{b}  \right\rangle := \sum_{i}^{} \left[ \textbf{a}  \right]_i \left( \left[ \textbf{b}  \right]_i \right)^{*},  \left\Vert \textbf{a}  \right\Vert ^{2}:= \sum_{i}^{}\left|  \left[ \textbf{a}  \right]_i   \right|^{2}.$
    For functions we have the integral inner product (with conjugate) and the squared $L^{2}$-norm
            $\left\langle x, y \right\rangle := \int_{-\infty}^{\infty} x\left( t \right)y^{*}\left( y \right)dt ,       \left\Vert  y \right\Vert ^{2}:= \int_{-\infty}^{\infty} \left| y\left( t \right) \right|^{2}dt.$

            If $\mathbb{T} $ is an operator then we can expand the norm $\left\Vert \mathbb{T} x  \right\Vert^{2} = x^{*}\mathbb{T} ^{*}\mathbb{T} x$ where $x^{*}$ is the conjugate of $x$, and $\mathbb{T} ^{*}$ is the adjoint of $\mathbb{T} $.
\end{sectionbox}

\begin{sectionbox}[Operator norms on spaces of bounded operators]
    The space $\mathcal{B} \left( \mathcal{X} , \mathcal{Y}  \right)$ of bounded linear operators (see subsection \ref{subsec:hilbert_spaces_linear_operators}) is equipped with a norm. If $\mathbb{T}  \in \mathcal{B} \left( \mathcal{X}, \mathcal{Y}  \right)$, then the operator norm is     $            \left\Vert \mathbb{T} \right\Vert = \sup_{\left\Vert x \right\Vert _\X = 1} \left\Vert \mathbb{T} x \right\Vert _{\Y}.$ 

    The operator norm satisfies these inequalities:
    \begin{enumeratenosep}
       \item For any $x \in \mathcal{X} $ $          \left\Vert \mathbb{T} x \right\Vert _\Y \le  \left\Vert \mathbb{T} \right\Vert \left\Vert x \right\Vert _\X.$
\item  For the composition $\mathbb{T} _1\mathbb{T} _2$ $\left\Vert \mathbb{T}_1 \mathbb{T}_2 \right\Vert \le  \left\Vert \mathbb{T}_1 \right\Vert \left\Vert \mathbb{T}_2 \right\Vert $
\end{enumeratenosep}

If $\mathbb{T} $ is self-adjoint, then the operator norm satisfies $\left\Vert \T \right\Vert = \sup_{\left\Vert x \right\Vert  = 1} \left\{ \left| \left\langle \T x, x \right\rangle  \right| \right\}.$     If $\mathbb{T} $ is a nonzero orthogonal projection (see subsection \ref{subsec:hilbert_spaces_projections}), then the operator norm satisfies $\left\Vert \T \right\Vert = 1.$ (Proof \ref{proof:orthogonal_projection_operator_norm})
\end{sectionbox}


\begin{sectionbox}[Norms on matrices]
    Let $A$ be a matrix.
    \begin{enumeratenosep}[i.]
                    \item         $\left\vvvert A \right\vvvert_2 $ is the operator 2-norm $                      \left\vvvert A \right\vvvert _2 = \max _{x : \left\Vert x \right\Vert _2 = 1} \left\Vert Ax \right\Vert _2. $ This is the greatest singular value of $A$, so the square root of the greatest eigenvalue of $A^{*}A$.
              \item $\left\Vert A \right\Vert _2$ is the Frobenius norm $                      \left\Vert A \right\Vert _2 = \sqrt{\text{tr} \left( AA^{H} \right)} .$ This is the square root of the sum of squares of singular values of $A$.
    \end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[Norms on orthonormal bases]
    $\mathcal{B}$ be a countable orthonormal basis for $\mathcal{H} $. Denote $g_j$ as the elements of $\mathcal{B}  = \left\{ g_j \right\}_{j \in \mathbb{N} }$. Then the norm $\left\Vert x \right\Vert _{\mathcal{B} , p}$ is $    \left\Vert x \right\Vert _{\mathcal{B} , p} = \left( \sum_{j = 0}^{\infty} \left| \left\langle x, g_j \right\rangle  \right|^{p} \right)^{1/p}.$
\end{sectionbox} 



\subsection{Properties of vector spaces, $\left\Vert \cdot \right\Vert $ and $\left\langle \cdot, \cdot \right\rangle $}\label{subsec:hilbert_spaces_connections_norms_products}

\begin{sectionbox}[Unions and intersections of vector spaces]
    \begin{enumeratenosep}
        \item An intersection of vector spaces is a vector space.
        \item A union of vector spaces might not be a vector space.
    \end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[Norms and inner products are continuous]
    Suppose $\lim_{n \to \infty} x_n =x$.
    \begin{enumeratenosep}
        \item   $\left\Vert \cdot \right\Vert $ is continuous, so $\lim_{n \to \infty} \left\Vert x_n \right\Vert = \left\Vert x \right\Vert. $
        \item      $\left\langle \cdot, \cdot \right\rangle $ is continuous, so $\lim_{n \to \infty}  \left\langle x_n, y_n \right\rangle  =  \left\langle x, y \right\rangle . $

    \end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[Closest point]
    Suppose $\S$ is a closed subspace of a Hilbert space $\X$, and take some $x \in \X$. The closest point  $y \in \mathcal{S} $ in $\mathcal{S} $ is $    \left\Vert x - y \right\Vert  = \min_{z \in \S}\left\Vert x - z \right\Vert $. It always exists. $\left( x - y \right)$ is orthogonal to all points in $\mathcal{S} $.
\end{sectionbox}


\begin{sectionbox}[Induced norm]
    \begin{enumeratenosep}[label=\arabic*]
        \item An inner product $\left\langle \cdot, \cdot \right\rangle $ on $\mathcal{X} $ induces a norm $\left\Vert x \right\Vert  = \sqrt{\left\langle x, x \right\rangle } $.
        \item If a norm on $\mathbb{C} $ is induced by an inner product, we can retrieve the inner product by
                $\left\langle x, y \right\rangle=\frac{1}{4} \left( \left\Vert x + y \right\Vert ^{2} - \left\Vert x - y \right\Vert ^{2} + i\left\Vert x + iy \right\Vert ^{2} - i\left\Vert x - iy \right\Vert ^{2} \right)$
    \end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[Parallelogram law]
    Norms induced by an inner product (most norms we use) satisfy the parallelogram law. For all $x, y \in \X$ $  2\left\Vert x \right\Vert ^{2} + 2\left\Vert y \right\Vert ^{2} = \left\Vert x + y \right\Vert ^{2} + \left\Vert x - y \right\Vert ^{2}.$
\end{sectionbox}

\begin{sectionbox}[Cauchy-Schwarz inequality]
    Let $\X$ be an inner product space. Then for all $x, y \in \X$, $    \left| \left\langle x, y \right\rangle  \right| \le \left\Vert x \right\Vert \left\Vert y \right\Vert$ with equality if and only if $x$ and $y$ are linearly dependent.
\end{sectionbox}

% SKIP: Continuity of norm and inner product

\subsection{Linear independence, orthogonality, spans}\label{subsec:hilbert_spaces_orthonormal_basis}

\begin{sectionbox}[Linear span]
   The subspace spanned by $\S$ is the intersection of all subspaces containing $\S$. It can be written as the set of all finite combinations of elements of $\S$, so
\[
    \text{span} \left( \S \right) \triangleq \left\{ \sum_{l = 1}^{k} \lambda _l x_l : k \in \N, x_l \in \S, \lambda _l \in \F \right\}.
\]
\end{sectionbox}

\begin{sectionbox}[Linear independence]
    The set of vectors $\left\{ x_1. x_2, \dots, x_N \right\}$ is linearly independent if for every $\lambda _1, \lambda _2, \dots, \lambda _N \in \F$, we have the implication
\[
    \lambda _1x_1 + \lambda _2 + x_2 + \cdots + \lambda _N + x_N = 0 \implies \lambda _1 = \lambda _2 = \cdots = \lambda _N = 0.
\]

If $\S$ is infinite, then it is linearly independent if every finite subset of $\S$ is linearly independent.
\end{sectionbox}

\begin{sectionbox}[Dimensionality of vector spaces]
    $\X$ is $N$-dimensional if there exist $N$ linearly independent vectors in $\X$ and any $N + 1$ vectors in $\X$ are linearly dependent.
\end{sectionbox}


\begin{sectionbox}[Orthogonality between vectors and sets]
    We say that the vectors $x$ and $y$ are orthogonal, written as $x \perp y$, if
\[
    \left\langle x, y \right\rangle  = 0.
\]

We say that the sets $\U$ and $\V$ are orthogonal if for all $x \in \U$ and $y \in \V$, $x \perp y$.

The orthogonal complement $S^{\perp}$ is defined as
\[
    S^{\perp} := \left\{ x \in \X: x \perp  \S \right\}.
\]
The orthogonal complement is a vector subspace of $\mathcal{X} $.
\end{sectionbox}

\begin{sectionbox}[Biorthormality]
  Two sets of vectors $\left\{ \textbf{e} _1, \textbf{e} _2  \right\}$ and $\left\{ \tilde{\textbf{e} }_1, \tilde{\textbf{e} }_2 \right\}$ are biorthonormal if they satisfy
\[
    \left\langle \textbf{e} _j, \tilde{\textbf{e} }_k \right\rangle =
    \begin{cases}
        1, \text{ if }j = k, \\
        0, \text{ otherwise} 
    \end{cases} \text{ for $j, k = 1, 2$.} 
\]
\end{sectionbox}


\begin{sectionbox}[Orthogonal and orthonormal sets]
A subset $\S$ of nonzero vectors of a Hilbert space $\X$ is orthogonal if any two distinct elements in $\S$ are orthogonal.

An orthogonal set $\S$ is called orthonormal if also $\left\Vert x \right\Vert  = 1$ for all $x \in \S$.
\end{sectionbox}

\begin{sectionbox}[Relationship between span and orthogonal complement]
   Below,  $\overline{\mathcal{S}} $ is the closure of $\mathcal{S} $.
    \begin{enumeratenosep}[i.]
    \item $S^{\perp} = \overline{\text{span}\left( \S \right)}^{\perp}$,
    \item $S^{\perp\perp} = \overline{\text{span}\left( \S \right)}$,
    \item $S^{\perp} = \left\{ 0 \right\}$ if and only if $\overline{\text{span}\left( \S \right)} = \X$.
\end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[Complete subset for a Hilbert space]
    $\G = \left\{ g_k \right\}_{k \in \K}$, $g_k \in \mathcal{H} $, $k \in \K$ is complete for a Hilbert space $\mathcal{H} $ if $\left\langle x, g_k \right\rangle  = 0$ for all $k \in \K$ and with $x \in \mathcal{H} $ implies $x = 0$. That is, the only element in $\mathcal{H} $ that is orthogonal to $\G$ is $x = 0$.
\end{sectionbox}


\subsection{Orthonormal basis and its properties}\label{subsec:hilbert_spaces_orthonormal_basis}

\begin{sectionbox}[Orthonormal basis for $\mathcal{X}$]
    An orthonormal basis (ONB) of a Hilbert space $\mathcal{X} $ is an orthonormal subset of $\mathcal{X} $ which satisfies any of these equivalent conditions:
    \begin{enumeratenosep}
            \item $\left\langle x, x_\alpha  \right\rangle  = 0$ for all $\alpha  \in \I$ implies that $x = 0$,
    \item $x = \sum_{\alpha  \in \I}^{} \left\langle x, x_\alpha  \right\rangle x_\alpha $ for all $x \in \X$,
    \item $\left\Vert x \right\Vert ^{2} = \sum_{\alpha  \in \I}^{} \left| \left\langle x, x_\alpha  \right\rangle  \right|^{2}$ for all $x \in \X$,
    \item $\left[ \S \right] = \X$,
    \item $\S$ is a maximal orthonormal set, i.e. it has the property that if $\S' \supset \S$ is another orthonormal set, then necessarily $\S' = \S$.
    \end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[Representing $x \in \mathcal{X} $ with ONB]
    If $S = \left\{ x_\alpha  \in \X | \alpha  \in \I \right\}$ is an orthonormal basis for $\mathcal{X} $, then we can represent any $x \in \mathcal{X} $ by \[x = \sum_{\alpha  \in \mathcal{I}}^{} \left\langle x, x_\alpha  \right\rangle x_\alpha.\]
\end{sectionbox}

\begin{sectionbox}[Parseval's identity]
     If $S = \left\{ x_\alpha  \in \X | \alpha  \in \I \right\}$ is an orthonormal basis for $\mathcal{X} $, then for any $x \in \mathcal{X} $ it's true that
\[
    \left\Vert x \right\Vert ^{2} = \sum_{\alpha \in \I}^{}  \left| \left\langle x, x_\alpha  \right\rangle  \right|^{2}
\]
\end{sectionbox}

\subsection{Linear operators and their properties}\label{subsec:hilbert_spaces_linear_operators}

\begin{sectionbox}[Key definitions]
    \begin{enumeratenosep}
    \item A linear operator from $\X$ to $\Y$ is a function $\mathbb{T}  : \X \to \Y$ that satisfies
\[
    \mathbb{T} \left( \alpha x + \beta y \right) = \alpha \mathbb{T} x + \beta \mathbb{T} y
\] for all $x, y \in \X$ and $\alpha , \beta  \in \F$.
    \item 
The range of $\mathbb{T}$ is the set $\mathcal{R}\left( \mathbb{T} \right) = \left\{ \mathbb{T} x : x \in \X \right\}$. The range $\mathcal{R} \left( \mathbb{T}  \right)$ is a subspace of $\Y$,
\item The rank of $\mathbb{T}$ is the dimension of $\mathcal{R}\left( \mathbb{T} \right)$.
\item The null space of $\mathbb{T}$ is the set $\No\left( \mathbb{T} \right) = \left\{ x \in \X : \mathbb{T} x = 0 \right\}$. The null space $\No \left( \T \right)$ is a subspace of $\X$.

\item The kernel is the same as the null space.
    \end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[Bounded linear operator]
    A linear operator $\mathbb{T} : \X \to \Y$ is bounded if there exists a constant $K > 0$ such that for all $x \in \X$,    $\left\Vert \mathbb{T} x \right\Vert _\Y \le K \left\Vert x \right\Vert _\X.$

    $\B \left( \X, \Y \right)$ is the set of bounded linear operators $\mathbb{T} : \X \to \Y$.
\end{sectionbox}

\begin{sectionbox}[Continuity of linear operators]
    The following are equivalent:
\begin{enumeratenosep}[a)]
    \item $\T$ is continuous,
    \item $\left\Vert \T \right\Vert  < \infty$,
    \item $\left\Vert \T \right\Vert $ is bounded.
\end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[Linear isometries]
    $\T$ is a linear isometry if $\left\Vert \T x \right\Vert _\Y = \left\Vert x \right\Vert _\X$ for all $x \in \X$. In cases of matrices, these could be understood to be rotations.
\end{sectionbox}

\subsection{Projections}\label{subsec:hilbert_spaces_projections}

\begin{sectionbox}[Projection on a vector space]
    A projection on a vector space $\X$ is a linear operator $\P : \X \to \X$ such that $\P^{2}=\P$. 
    
        The projection onto $\S$ along $\Tc$ is the unique projection $\P : \X \to \X$ such that $\S = \Rc \left( \P \right)$ and $\Tc = \No\left( \P \right)$ where the direct sum $\S \oplus \Tc$ is defined by    $\S \oplus \Tc = \left\{ x + y | x \in \S, y \in \Tc \right\}$ when $\S \cap \Tc = \left\{ 0 \right\}$.
\end{sectionbox}

\begin{sectionbox}[Range and null space of a projection sum up to $\mathcal{X} $]
        The direct sum of the range of $\mathbb{P} $ and its null space sum up to $\mathcal{X} $: $\X = \Rc \left( \P \right) \oplus \No\left( \P \right)$.
\end{sectionbox}

\begin{sectionbox}[Decomposing $x \in \mathcal{X} $ by projections]
    Suppose $\mathbb{P} $ is a projection onto $\mathcal{S} $ along $\mathcal{T} $. Then we can decompose any $x \in \mathcal{X} $ uniquely into $x = s + t$ with $s \in \S$ and $t \in \Tc$, and the projection itself can then be written $\mathbb{P} x = s$.
\end{sectionbox}


\begin{sectionbox}[Orthogonal projection on a Hilbert space]
    An orthogonal projection on a Hilbert space $\X$ is a linear operator $\P : \X \to \X$ such that $\P^{2} = \P$ and
\[
    \left\langle \P x, y \right\rangle = \left\langle x, \P y \right\rangle \text{ for $x, y \in \X$.} 
\]

Note that the above definition directly means that every orthogonal projection $\mathbb{P} $ is self-adjoint.
\end{sectionbox}

\begin{sectionbox}[Operator norms of orthogonal projections]
    If $\mathbb{P} $ is a non-zero orthogonal projection then $\left\Vert \P \right\Vert  = 1$
\end{sectionbox}

\begin{sectionbox}[Orthogonal projections as sums]
    Suppose $\S = \left\{ x_\alpha : \alpha \in \I \right\}$ is an orthonormal system in a Hilbert space $\X$. Let $\V = \left[ \S \right]$ be the closed linear span of $\S \subset \mathcal{X} $.     We can write the projection $\P_\V$ onto $\V$ explicitly as
\[
    \P_\V x = \sum_{\alpha \in \I}^{}  \left\langle x, x_\alpha  \right\rangle x_\alpha.
\]
\end{sectionbox}



\subsection{Bounded linear operators on Hilbert spaces}\label{subsec:hilbert_spaces_bounded_operators}

\begin{sectionbox}[Riesz representation theorem]
    Let $f : \X \to \F$ be a bounded linear functional on a Hilbert space $\X$. Then $f\left( x \right)$ can be represented as an inner product for some constant $y_f \in \mathcal{X} $, i.e. there exists a unique $y_f \in \X$ such that $f(x) = \left\langle x, y_f \right\rangle $ for all $x \in \X$.
\end{sectionbox}

\begin{sectionbox}[Adjoint operator]
            The adjoint $\T^{*} \in \B\left( \Y, \X \right)$ is the unique operator such that
        \[
            \left\langle \T x, y \right\rangle = \left\langle x, \T^{*}y \right\rangle , \text{ $x \in \X$, $y \in \Y$} 
        \] with $\left\Vert \T^{*} \right\Vert = \left\Vert \T \right\Vert $. Furthermore, $\T^{**} = \T$.

        $\T$ is self-adjoint if $\T = \T^{*}$. The operator norm of a self-adjoint operator satisfies
    \[\left\Vert \T \right\Vert = \sup_{\left\Vert x \right\Vert  = 1} \left\{ \left| \left\langle \T x, x \right\rangle  \right| \right\}.\]

    $\T$ is called normal if $\T\T^{*} = \T^{*}\T$.
\end{sectionbox}

\begin{sectionbox}[Singular values]
    Consider a matrix $A$.  Its singular values $\sigma _i$ are the square roots of the eigenvalues of $A^{*}A$. If $A$ is self-adjoint (or, more generally, normal), then its singular values $\sigma _1$ are its absolute eigenvalues $\left| \lambda _1 \right|$
\end{sectionbox}


\begin{sectionbox}[Spectral theorem and eigenvalues of operators]
    If $\mathbb{T} $ is a self-adjoint operator, then by the spectral theorem its least and greatest eigenvalues satisfy
    \[
    \lambda _{\text{min} } = \inf _{x \in \mathcal{H} } \frac{\left\langle \mathbb{T} x, x \right\rangle }{\left\Vert x \right\Vert ^{2}} \text{ and } \lambda _{\text{max}} = \sup _{x \in \mathcal{H} } \frac{\left\langle \mathbb{T} x, x \right\rangle }{\left\Vert x \right\Vert ^{2}}.
\]

[TODO: Verify] For any $\mathbb{B} T$, $\mathbb{T} ^{*}\mathbb{T} $ is a self-adjoint operator, we have that
\[
                \lambda _{\text{min} } \left\Vert x \right\Vert _2^{2} \le x^{*}\mathbb{T}^{*}\mathbb{T} x \le \lambda _{\text{max}} \left\Vert x \right\Vert _2^{2}.
\]
\end{sectionbox}


\begin{sectionbox}[Unitary operator]
    $\T$ is unitary if it is invertible and if $\T^{-1} = \T^{*}$. Unitary operators are linear isometries, e.g. rotations.

    For a unitary operator $\mathbb{T} : \mathcal{H}  \to \mathcal{H} $ and $x \in \mathcal{H} $, it is true that $\left\Vert \mathbb{T} x \right\Vert_\mathcal{H}  = \left\Vert x \right\Vert _\mathcal{H} $.

    Parseval's theorem: Given $x \in \mathcal{H} $ in signal space and a corresponding sequence $\left\{ c_k \right\}_{k \in \mathcal{K} } := \mathbb{T} x$ given by a unitary transformation $\mathbb{T} $, it is true that $\left\Vert x \right\Vert _{L^{2} }^{2} = \left\Vert c \right\Vert _{l^{2}}^{2}$. (Proof \ref{proof:parseval_unitary})
\end{sectionbox}

\begin{sectionbox}[Positive (semi)definite operators]
            The self-adjoint operator $\T$ is
        \begin{enumeratenosep}[1)]
    \item positive semidefinite if $\left\langle \T x, x \right\rangle  \ge 0$ for all $x \in \X$. In this case, we write $\T \ge 0$.
    \item positive definite if it is positive semidefinite and $\left\langle \T x, x \right\rangle  = 0$ implies that $x = 0$. In this case, we write $\T > 0$.
\end{enumeratenosep}

A positive semidefinite operator has a unique, positive-definite square root $\mathbb{A} $ satisfying $\mathbb{T}  = \mathbb{A}^{2}$.
\end{sectionbox}

\begin{sectionbox}[Square roots of operators]
    $\Ab$ is a square root of $\T$ if $\T = \Ab^{2}$.  If $\mathbb{T} $ is a bounded linear operator from $\mathcal{X} $ onto $\mathcal{X} $ ($\T \in \B\left( \X, \X \right)$) and $\mathbb{T} $ is positive semidefinite  ($\T \ge 0$), then the square root exists and is unique. Furthermore, it is positive definite.
\end{sectionbox}


\subsection{Extras}\label{subsec:hilbert_spaces_extras}

\begin{sectionbox}[Closed spaces; closures]
    $\S$ is closed if it contains all of its limit points.     The closure $\overline{\S}$ is the intersection of all closed subsets of $\X$ which are supersets of $\S$.
\end{sectionbox}

\begin{sectionbox}[Cauchy sequence]
    The sequence $\left( x_{n} \right)_{n \geq 1} $ is Cauchy if for every $\varepsilon  > 0$ there exists an integer $N\left( \varepsilon  \right)$ such that $\left\Vert x_n - x_m \right\Vert  \le  \varepsilon $ whenever $n, m \ge N\left( \varepsilon  \right)$.
\end{sectionbox}

\begin{sectionbox}[Complete subset for a Hilbert space]
    $\G = \left\{ g_k \right\}_{k \in \K}$, $g_k \in \mathcal{H} $, $k \in \K$ is complete for a Hilbert space $\mathcal{H} $ if $\left\langle x, g_k \right\rangle  = 0$ for all $k \in \K$ and with $x \in \mathcal{H} $ implies $x = 0$. That is, the only element in $\mathcal{H} $ that is orthogonal to $\G$ is $x = 0$.
\end{sectionbox}

\begin{sectionbox}[Complete space \& completion]
    \emph{The definition below is not really used explicitly much in the course. You probably want the definition in the box just above.}

    $\X$ is a complete space if $\X$ is a normed space and if every Cauchy sequence in $\X$ converges in $\X$.

    The completion of $\X$ is a Banach space $\widehat{\X}$ with norm $\widehat{\left\Vert \cdot \right\Vert }$ such that    $\X$ is a subspace of $\widehat{\X}$,    on $\X$, $\widehat{\left\Vert \cdot \right\Vert } = \left\Vert \cdot \right\Vert $,    $\widehat{\X}$ is the closure of $\X$.
\end{sectionbox}


\begin{sectionbox}[Hilbert and Banach space]
    \begin{enumeratenosep}
            \item A Banach space is a space with a norm which is complete.
            \item A Hilbert space is a space with an inner product which is complete.
    \end{enumeratenosep}

    A Hilbert space is separable if it has a countable orthonormal basis.
\end{sectionbox}

\begin{sectionbox}[Dense space]
   $\A$ is dense in $\B$ if for the closure of $\A$ we have $\overline{\A} = \B$. Alternatively, $\A$ is dense in $\B$ if for every $x \in \B$ and every $\varepsilon  > 0$ there exists a $y \in \A$ such that $\left\Vert x - y \right\Vert  < \varepsilon $.
\end{sectionbox}

\begin{sectionbox}[Convergence of unordered sums]
    The definitions below are a bit obscure. In practice, if you have a sum over an index set $\mathcal{I} $, two things to keep in mind about convergence:
    \begin{enumeratenosep}
        \item  $\sum_{\alpha \in \I}^{} x_\alpha $ converges unconditionally if and only if $ \sum_{\alpha \in \I}^{}  \left\Vert x_\alpha  \right\Vert ^{2}< \infty.$
        \item The sum will not converge if it sums over an uncountable number of nonzero elements. It must be at most countable (e.g. $\mathbb{N} $). This is usually not a problem; almost all sums we deal with are over at most countable sets.
    \end{enumeratenosep}

    Formal definitions:
    Let $\left\{ x_\alpha \in \X | \alpha \in \I \right\}$ be an indexed subset of a normed space $\X$, where $\I$ may be countable or not. 
    \begin{enumeratenosep}
   \item  The unordered sum $\sum_{\alpha \in \I}^{} x_\alpha $ converges to $x \in \X$ if for every $\varepsilon  > 0$ there exists a finite subset $\J_\varepsilon \subseteq \I$ such that $\left\Vert S_\J - x \right\Vert < \varepsilon $ for all finite subsets $\J \subseteq \I$ containing $\J_\varepsilon $.
   \item  An unordered sum $\sum_{\alpha \in \I}^{} x_\alpha $ is Cauchy if for every $\varepsilon  > 0$ there exists a finite subset $\J_\varepsilon \subseteq \I$ such that the partial sums have $\left\Vert S_\K \right\Vert < \varepsilon $ for all finite sets $\K \subseteq \I \setminus \J_\varepsilon $. 
    \end{enumeratenosep}
\end{sectionbox}

