\section{Approximation Theory}\label{sec:approximation_theory}

\begin{notebox}[Notation]
            Let $d \in \mathbb{N} , \Omega \subset \mathbb{R} ^{d}$, and consider the function class $\mathcal{C} \subset L^{2} \left( \Omega  \right)$. 
        \begin{enumeratenosep}[i.]
    \item 
Set of binary encoders of length $l$ : $ \mathcal{E} ^{l} := \left\{  E : C \to \left\{ 0, 1 \right\}^{l} \right\}.$
\item 
Set of binary decoders of length $l$: $   \mathcal{D} ^{l} := \left\{ D : \left\{ 0, 1 \right\}^{l} \to L^{2} \left( \Omega  \right) \right\}.$
\item Uniform error over the function class $\mathcal{C} $: $    \sup _{f \in \mathcal{C} } \left\Vert D\left( E\left( f \right) \right) - f \right\Vert _{L^{2} \left( \Omega  \right)} \le \varepsilon .$
\end{enumeratenosep}

\end{notebox}

\begin{sectionbox}[Minimax code length]
Let $d \in \mathbb{N}$, $\Omega \subset \mathbb{R} ^{d}$ and $C \subset L^{2} \left( \Omega  \right)$. Then, for $\varepsilon  > 0$ the minimax code length $L\left( \varepsilon , \mathcal{C}  \right)$ is
$    L\left( \varepsilon , \mathcal{C}  \right) := \min \left\{ l \in \mathbb{N} : \exists_{} \left( E, D \right) \in \mathcal{E} ^{l} \times \mathcal{D} ^{l} : \sup _{f \in \mathcal{C} } \left\Vert D\left( E\left( f \right) \right) - f \right\Vert _{L^{2} \left( \Omega  \right)} \le \varepsilon  \right\}.$
\end{sectionbox}

\begin{sectionbox}[Optimal (Kolmogorov) exponent]
    The optimal exponent $\gamma ^{*}\left( \mathcal{C}  \right)$ is defined as
\[
    \gamma ^{*}\left( \mathcal{C}  \right) := \sup \left\{ \gamma \in \mathbb{R} : L\left( \varepsilon , \mathcal{C}  \right) \in \mathcal{O} \left( \varepsilon ^{-1/\gamma } \right), \varepsilon \to 0 \right\}.
\]
\end{sectionbox}

\begin{sectionbox}[Coverings; entropy; packings]
  Let $\left( \mathcal{X} , \rho  \right)$ be a metric space. An $\varepsilon$-covering of $\mathcal{C}  \subset \mathcal{X} $ with respect to the metric $\rho $ is a set of points $\left\{ x_1, \dots, x_n  \right\} \subset \mathcal{C} $ such that for each $x \in \mathcal{C} $, there exists an $i \in \left[ 1, N \right]$ so that $\rho \left( x, x_i \right)\le \varepsilon $. The $\varepsilon $-covering number $N\left( \varepsilon , \mathcal{C} , \rho  \right)$ is the cardinality of the smallest $\varepsilon $-covering.

  Metric entropy is the $\log $ in base 2 of a covering number $\log _2 N\left( \varepsilon , \mathcal{C} , \rho  \right)$.

  An $\varepsilon $-packing for a compact set $\mathcal{C} \subset \mathcal{X} $ with respect to the metric $\rho $ is a set $\left\{ x_1, \dots, x_N  \right\}\subset \mathcal{C}  $ such that $\rho \left( x_, x_j \right) > \varepsilon $ for all $i \neq j$. The $\varepsilon $-packing number $M\left( \varepsilon , \mathcal{X} , \rho  \right)$ is the cardinality of the greatest $\varepsilon $-packing.
\end{sectionbox}

\begin{sectionbox}[Relationships between packings and coverings]
    Let $\left( \mathcal{X} , \rho  \right)$ be a metric space and $\mathcal{C} $ a convex set in $\mathcal{X} $. For all $\varepsilon  > 0$, the packing and the covering number are related according to
\[
    M\left( 2\varepsilon , \mathcal{C} , \rho  \right) \le N\left( \varepsilon , \mathcal{C} , \rho  \right) \le M\left( \varepsilon , \mathcal{C} , \rho  \right).
\] (Proof \ref{proof:coverings_and_packings_relationship})
\end{sectionbox}



\begin{sectionbox}[Encoder-decoder pair based on covering]
    Let $x \in \mathcal{C} $, $\varepsilon >0$ and consider a $\varepsilon $-covering of $\mathcal{C} $.
\begin{enumerate}[ ]
    \item Encoder: Map $x$ to the closest (in $\rho $) ball center $x_i$. Produce a binary representation of $x$ by assigning it to the binary representation of $x_i$.

        $N\left( \varepsilon , \mathcal{C} , \rho  \right)$ is the number of ball centers. So, we need $\log _2 N\left( \varepsilon , \mathcal{C} , \rho  \right)$ bits to label (in binary form) the ball centers.
    \item Decoder: Take the bit string of length $\log _2 N\left( \varepsilon , \mathcal{C} , \rho  \right)$ and maps it to the center $x_i$.
\end{enumerate}

So, we get $D\left( E\left( x \right) \right)= x_i$. So, the approximation error is $\rho \left( x, x_i \right) \le \varepsilon $ by definition of the covering.

\end{sectionbox}


\begin{sectionbox}[Bounds on cross-norm covering numbers]
    Lemma 10.5: Consider the norms $\left\Vert \cdot \right\Vert $, $\left\Vert \cdot \right\Vert '$ on $\mathbb{R} ^{d}$, and let $\mathcal{B} , \mathcal{B} ' $ be their corresponding unit balls, i.e.
\[\begin{aligned}
    \mathcal{B} & = \left\{ x \in \mathbb{R} ^{d}: \left\Vert x \right\Vert  \le 1 \right\}\\
    \mathcal{B} ' &=\left\{ x \in \mathbb{R} ^{d} : \left\Vert x \right\Vert ' \le 1 \right\}.
\end{aligned}\]
\end{sectionbox}
\begin{sectionbox}
Then, the $\varepsilon $-covering number of the ball $\mathcal{B} $ with respect to the norm $\left\Vert \cdot \right\Vert '$ satisfies
\[
    \left( \frac{1}{\varepsilon } \right)^{d} \frac{\text{vol} \left( \mathcal{B}  \right)}{\text{vol} \left( \mathcal{B} ' \right)} \le N\left( \varepsilon , \mathcal{B}, \left\Vert \cdot \right\Vert ' \right) \le \frac{\text{vol} \left( \frac{2}{\varepsilon } \mathcal{B}  +\mathcal{B} ' \right)}{\text{vol} \left( \mathcal{B} ' \right)} .
\] (Proof \ref{proof:cross_norm_covering_numbers})
\end{sectionbox}

\subsection{Best $M$-term approximations}


\begin{sectionbox}[Best $M$-term approximation]
    Given a function class $\mathcal{C}  \subset L^{2} \left( \Omega  \right)$ and a dictionary  $\mathcal{D}  = \left( \phi _i \right)_{i \in I} \subset L^{2} \left( \Omega  \right)$ we define for $f \in \mathcal{C} $, the number of terms in the approximation $M \in \mathcal{N}$, $    \Gamma _M^{\mathcal{D} }\left( f \right) = \inf _{\substack{I_M \subset I \\ \#I_M = M, \left( c_i \right)_{i \in I_M}} } \left\Vert f = \sum_{i \in I_M}^{} c_i\phi _i \right\Vert _{L^{2} \left( \Omega  \right)}.$

We call $\Gamma ^{\mathcal{D} }_M\left( f \right)$ the best $M$-term approximation error of $f$ in $\mathcal{D} $.

\end{sectionbox}

\begin{sectionbox}[Best $M$-term approximation rates]
    The best $M$-term approximation rate of $\mathcal{C} $ in the representation system $\mathcal{D} $ is the supremal $\gamma >0$ such that
\[
    \sup _{f\in \mathcal{C} }\Gamma _M^{\mathcal{D}  }\left( f \right) \in \mathcal{O} \left( M^{-\gamma } \right), M \to \infty,
\] and is denoted as $\gamma ^{*}\left( \mathcal{C} , \mathcal{D}  \right)$.

The effective version of the above is restricted to a polynomial depth search, and is the largest $\gamma >0$ for which
\[
    \sup _{f \in \mathcal{C} }\inf _{\substack{I_M \subset \left\{ 1, \dots, \pi \left( M \right) \right\} \\ \#I_M = M, \left( c_i \right)_{i \in I_M}} } \left\Vert f - \sum_{i \in I_M}^{} c_i\phi _i \right\Vert    _{L^{2} \left( \Omega  \right)} \in \mathcal{O} \left( M^{-\gamma } \right) \text{ as } M \to \infty
\] holds, where $\pi $ is a polynomial. It is denoted by $\gamma ^{*, \text{eff} }\left( \mathcal{C} , \mathcal{D}  \right)$.
\end{sectionbox}

\begin{sectionbox}[Best $M$-term approximation and Kolmogorov rate]
    Theorem 10.8:  Let $d \in \mathbb{N} $ and $\Gamma \subset \mathbb{R} ^{d}$. The effective (polynomial depth-search) best $M$-term approximation rate of the function class $\mathcal{C}  \subset L^{2} \left( \Gamma  \right)$ in the representation system dictionary $\mathcal{D}  \subset L^{2} \left( \Omega  \right)$ satisfies
\[
    \underbrace{\gamma ^{*, \text{eff} } \left( \mathcal{C} , \mathcal{D}  \right)}_{\text{polynomial depth-search} } \le \underbrace{\gamma ^{*}\left( \mathcal{C}  \right)}_{\text{Kolmogorov exponent} }.
\]
\end{sectionbox}

% SKIP proof

\begin{sectionbox}[Approximation error]
    Let $\mathcal{B} = \left\{ g_j \right\}_{j \in \mathbb{N} _0}$ be an orthonormal basis for a Hilbert space $\mathcal{H} $.  Consider a linear $M$-term approximation of $x \in \mathcal{H} $ $ x_M = \sum_{j = 0}^{n - 1} \left\langle x, g_j \right\rangle g_j.$ The approximation error $\mathcal{E} _l \left[ M \right]$ is defined as    $\mathcal{E} _l \left[ M \right] =  \left\Vert x - x_M \right\Vert ^{2} $
\end{sectionbox}


\begin{sectionbox}[Linear $M$-term approximation error rates]
Theorem 11.1: Let $s > 1/2$ and $\sum_{j = 0}^{\infty} j^{2s} \left| \left\langle x, g_j \right\rangle  \right|^{2} <\infty$. There exist constants $A, B > 0$ such that $   A\sum_{j = 0}^{\infty} j^{2s}\left| \left\langle x, g_j \right\rangle  \right|^{2} \le \sum_{M = 0}^{\infty} M^{2s - 1} \mathcal{E} _l \left[ M \right] \le B \sum_{j = 0}^{\infty} j^{2s}\left| \left\langle x, g_j \right\rangle  \right|^{2}$ and hence $\mathcal{E} _l\left[ M \right] = o\left( M^{-2s} \right)$, i.e. $\lim_{M \to \infty} \mathcal{E} _l\left[ M \right]M^{2s}=0$.
\end{sectionbox}

\begin{sectionbox}[Nonlinear approximation takes indices with greatest absolute coefficients]
    Let $\mathcal{B} = \left\{ g_j \right\}_{j \in \mathbb{N} } $ be an orthonormal basis for $\mathcal{H} $. Fix an integer $M$, take $x \in \mathcal{H} $, and define  $x_M = \sum_{j \in I_M}^{} \left\langle x, g_j \right\rangle g_j.$ To minimize the error $    \mathcal{E} \left[ M \right] = \left\Vert x - x_M \right\Vert ^{2} $ it is necessary to take the indices of $x$ with the greatest coefficients $\left| \left\langle x, g_j \right\rangle  \right|$ into $I_M$. (Proof \ref{proof:nonlinear_greatest_coefficients})

\end{sectionbox}

\begin{sectionbox}[Best $M$-term approximation in term of ranked coefficients]
    Sort $\left[ \left| \left\langle x, g_j \right\rangle  \right| \right]_{j \in \mathbb{N} }$ in descending order and denote the rank ordered coefficients $   x_{\mathcal{B} }^{r}\left[ k \right] = \left\langle x, g_{j_k} \right\rangle .$
    So $\left| x_{\mathcal{B} }^{r}\left[ k \right] \right| \ge \left| x_{\mathcal{B} }^{r}\left[ k + 1 \right] \right|$, $k \ge 1$.

The best $M$-term approximation of $x$ is thus $    x_M = \sum_{k = 1}^{M} x_{\mathcal{B} }^{r}\left[ k \right]g_{j_k}.$
\end{sectionbox}

\begin{sectionbox}[Error rates from decay condition on ranked coefficients]
There are two theorems of relevance here. 11.5 should generally be easier to show.

    Theorem 11.4: Let $s > 1/2$. If there exists $C > 0$ such that your rank ordered coefficients satisfy a decay condition
\[
    \left| x_\mathcal{B} ^{r}\left[ k \right] \right| \le Ck^{-s}
\] then the nonlinear approximation error can be upper bounded as
\[
    \mathcal{E} _n \left[ M \right] \le \frac{C^{2}}{2s - 1}M^{1 - 2s}.
\]
Conversely, if $\mathcal{E} _n \left[ M \right]$ satisfies the above inequality, then there is a decay condition
\[
    \left| x_\mathcal{B} ^{r}\left[ k \right] \right| \le \left( 1 - \frac{1}{2s} \right)^{-s}Ck^{-s}.
\]
\end{sectionbox}
\begin{sectionbox}

The theorem below relies on the norm on an orthonormal basis $\mathcal{B} $, defined in \nameref{sec:hilbert_spaces}.

Theorem 11.5: Let $p < 2$. If $\left\Vert x \right\Vert _{\mathcal{B} , p} < \infty$ then we get a decay condition
\[
    \left| x_\mathcal{B} ^{r}\left[ k \right] \right| \le \left\Vert x \right\Vert _{\mathcal{B} , p}k^{-1/p}
\] and
\[
    \mathcal{E} _n\left[ M \right] = o\left( M^{1-2/p} \right).
\]
\end{sectionbox}

\begin{sectionbox}[Space of functions well approximated by few elements of an ONB]
    Using theorem 11.5, we can define a space $\mathcal{B} _{\mathcal{B} , p}$ of all functions well approximated by a few elements of $\mathcal{B} $: $\mathcal{B} _{\mathcal{B} , p} = \left\{ x \in \mathcal{H} :\left\Vert x \right\Vert _{\mathcal{B} , p} < \infty \right\}$
\end{sectionbox}








