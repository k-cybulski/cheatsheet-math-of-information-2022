\section{Finite Rate of Innovation and ESPRIT}\label{sec:fri_esprit}

\begin{sectionbox}[Finite Rate of Innovation problem]
The Finite Rate of Innovation problem finds the $c_k$ and $t_k$ of a signal
\[
    x(t) = \sum_{k = 0 }^{K - 1} c_k \delta \left( t - t_k \right) \text{ for }t \in \R 
\]
where $\delta (t)$ is the Dirac delta distribution.
\end{sectionbox}

\begin{sectionbox}[Convolutions]
    \begin{enumeratenosep}[i.]
            \item For continuous time signals: $y\left( t \right) = \left( x * h \right)\left( t \right) = \int_{-\infty}^{\infty} x\left( \tau  \right)h\left( t - \tau  \right)d\tau = \int_{-\infty}^{\infty} h\left( \tau  \right)x\left( t - \tau  \right)d\tau $
        \item For discrete time signals: $c\left[ n \right] = \left( a * b \right)\left[ n \right] = \sum_{l = -\infty}^{\infty} a\left[ l \right]b\left[ n - l \right] = \sum_{l = -\infty}^{\infty} b\left[ l \right] a\left[ n - l \right]$
    \end{enumeratenosep}

        The Fourier transform maps convolutions to products, so if $y\left( t \right) = \left( x * h \right)\left( t \right)$ then $    \hat{y}\left( f \right) = \hat{x}\left( f \right) \hat{h}\left( f \right).$

\end{sectionbox}

\begin{sectionbox}[$z$-transform]
  The $z$-transform of a sequence $\left( a_{n} \right)_{n \in \mathbb{Z} } $ is given by $A\left( z \right) = \sum_{n = -\infty}^{\infty} a_nz^{-n}$.

\end{sectionbox}

\begin{sectionbox}[Fourier series representation in FRI]
    Given a finite-length sequence consisting of $K$ Dirac impulses $    x(t) = \sum_{k = 0 }^{K - 1} c_k \delta \left( t - t_k \right) \text{ for }t \in \R $ the FRI algorithm requires us to represent it by a Fourier series expansion of $x\left( t \right)$ as if it was a periodic function with period $\tau  = \max _k t_k$.
\[\begin{aligned}
    d_n &= \frac{1}{\tau}\int_{0}^{\tau } x\left( t \right) e^{-i2\pi n\frac{t}{\tau }}dt\\
        &= \frac{1}{\tau } \sum_{k = 0}^{K - 1} c_k e^{-i2\pi n\frac{tk}{\tau }} \text{ for $n \in \mathbb{Z} $.} 
\end{aligned}\]

\end{sectionbox}


\begin{sectionbox}[Annihilating filter method]
    The Finite Rate of Innovation algorithm relies on a filter sequence $a_n$ which convolves with the Fourier series expansion $d_n$ as
    $d_n * a_n = 0$. The $z$-transform of $a_n$ is
    $    A\left( z \right) = \sum_{m = 0}^{K} a_nz^{m} = \prod_{i = 0}^{K - 1} \left( 1 - e^{-i2\pi \frac{t_k}{\tau }}z^{-1} \right).$ (Proof \ref{proof:annihilating_filter_method})

\end{sectionbox}

\begin{sectionbox}[Finding $a_n$ from null space of $S$]
    The sequence $a_n$ is found by arranging the $d_n$ into a Hankel matrix $\textbf{S} $
    \[
    \textbf{S}  = \begin{bmatrix}
        d_0 & d_{- 1} & \cdots & d_{-K}\\
        d_1 & d_0 & \cdots & d_{-K + 1}\\
        \vdots & & & \vdots\\
        d_K & d_{K - 1} & \cdots & d_0
    \end{bmatrix}
\] and finding its null space. It has rank 1. (Proof \ref{proof:hankel_matrix_fourier_rank_1}) This null space can be found by e.g. Singular Value Decomposition. 
\end{sectionbox}


\begin{sectionbox}[Extracting $t_k$ from $A(z)$]
    We can extract $t_k$ from the zeros $z_k$ of $A\left( z \right)$, which are given by
\[\begin{aligned}
    1 - e^{-i2\pi \frac{t_k}{\tau }}z_k^{-1} &= 0\\
    t_k&= -\frac{\tau }{2\pi }\arg \left( e^{-i2\pi \frac{t_k}{\tau }} \right)
\end{aligned}\] and we can take $\arg $ in the last line because there is no phase unwrapping problem, since $t_k \in \left[ 0, \tau  \right)$.     $\arg \left( x \right)$ denotes the phase of $x$. If $x$ is on the unit circle, then it is the inverse of $\phi \mapsto e^{i\phi}$, so if $\left| x \right| = 1$ then it satisfies $            x = e^{i\arg \left( x \right)}. $

    
\end{sectionbox}

% SKIP how to find c_k

\begin{sectionbox}[Shift property of Vandermonde matrices]
    If $V$ is a matrix, then
    \begin{enumeratenosep}
            \item $V_{\uparrow}$ is $V$ with the top row removed.
    \item $V_{\downarrow}$ is $V$ with the bottom row removed.
    \end{enumeratenosep}

        Let $V_L$ be a Vandermonde matrix with nodes $z_1, \dots, z_K$, and $D_z$ a diagonal matrix with these same entries $z_1, \dots, z_K$ on the diagonal. Then $    V_{\uparrow} = V_{\downarrow}D_z$
\end{sectionbox}


\begin{sectionbox}[Similar matrices and their eigenvalues]
    Suppose matrices $A \in \Co^{n \times n}$ and $B \in \Co^{n \times n}$ are similar, i.e. there exists an invertible $n \times n$ matrix $P$ such that $A = P^{-1}BP$. Then $A$ and $B$ have the same eigenvalues with the same geometric multiplicities. (Proof \ref{proof:similar_matrices_eigenvalues})
\end{sectionbox}

\subsection{Summary of the ESPRIT algorithm}
Suppose you are given a sequence of $N$ observations $x_n$ which satisfy
\[
    x_n = \sum_{k = 1}^{K}  \alpha _k z_k^{n}
\] where $N \ge 2K$. The procedure below describes how we can find the nodes $z_1, \dots, z_k$. Let $\textbf{X} $ be a Hankel data matrix.
\begin{enumerate}[1.]
    \item Note that $\textbf{X} $ can be written in two ways
          \[\begin{aligned}
              \textbf{X} &= V_L D_\alpha V_{N - L + 1}\\
            \textbf{X} &= \text{SVD} \left( \textbf{X}  \right) = \textbf{S}  \Lambda \textbf{R} ^{H}
          \end{aligned}\] where the Vandermonde matrices $V$ have $z_1, \dots, z_k $ as their nodes.
      \item Hence, since the columns of $\textbf{X} $ are a linear combination of columns of both $V_L$ and of $\textbf{S}$, we get
          \[
              \text{span} \left( \textbf{X}  \right) = \text{span} \left( V_L \right) =\text{span} \left( \textbf{S}  \right) 
          \] and we can write $\textbf{S}  = V_LP$ for some full rank transformation $P$.
      \item Now, recall the shift property of Vandermonde matrices
          \[
    V_\uparrow = V_\downarrow D_z
          \] and note that
\[\begin{aligned}
    \textbf{S}_{\downarrow} &= V_{\downarrow} P & \implies V_{\downarrow} &= \textbf{S}_{\downarrow}P^{-1}\\
    \textbf{S}_{\uparrow} &=V_{\uparrow}P &\implies V_{\uparrow} &=\textbf{S}_{\uparrow}P^{-1}.
\end{aligned}\]
\item The above step gives us
\[\begin{aligned}
    \textbf{S}_\uparrow P^{-1} &= \textbf{S}_\downarrow P^{-1}D_z\\
    \textbf{S}_\uparrow &=\textbf{S}_\downarrow \underbrace{P^{-1}D_z P}_{=:\Phi}.
\end{aligned}\]
\item $\textbf{S} _\downarrow$ is rank $M$, which we can show by applying Sylvester's inequality to $V_{L - 1}P$, so its pseudo-inverse satisfies $\textbf{A} ^{\dagger}\textbf{A} = \textbf{I} $. Hence, we get
\[\begin{aligned}
    \Phi &= \textbf{S} _\downarrow^{\dagger} \textbf{S} _\uparrow.
\end{aligned}\]
\item Now, finally, by the similarity principle, we have for the eigenvalues
    \[
        \lambda _i \left( \Phi \right)  = \underbrace{\lambda _i \left( P^{-1}D_zP\right) = \lambda _i \left( D_z \right)}_{\text{similarity principle} } = z_i.
    \]
\end{enumerate}

% SKIP: Finding roots of a polynomial by ESPRIT
