\section{Fourier transforms}\label{sec:fourier}

\begin{sectionbox}[Fourier transform definitions]
Continuous and discrete time Fourier transform:
    \begin{enumeratenosep}[i.]
\item  $  \hat{x}\left( f \right) := \int_{-\infty}^{\infty} x\left( t \right) e^{-i2\pi tf}dt = \left\langle x\left( \cdot \right), e^{i2\pi f \cdot} \right\rangle $
\item
    $\hat{x}_d (f) := \sum_{k }^{} x[k] e^{-i2\pi k f} = \sum_{k}^{} x(kT) e^{-2 \pi k f}$
    \end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[Nyquist-Shannon Sampling Theorem]
            Let $x\left( t \right)$ be a band-limited signal with bandwidth $B$. Suppose that the sampling frequency satisfies $1/T \ge 2B$. We can write the complete continuous signal $x\left( t \right)$ based on discrete samples $\left\{ x\left[ k \right]:=x\left( kT \right) \right\}_{k \in \mathbb{Z} }$ as $                        x\left( t \right)                                     =2BT \sum_{k = -\infty}^{\infty} x\left[ k \right]\text{sinc} \left( 2B \left( t - kT \right) \right). $ (Proof \ref{proof:nyquist_shannon_sampling_theorem})

\end{sectionbox}

\begin{sectionbox}[Fourier transform as frame expansion]
    We can write a signal $x\left( t \right) \in \mathcal{L} ^{2}\left( B \right)$ sampled with a rate satisfying $1/T \ge 2B$ as a frame expansion
                              $x\left( t \right) = T \sum_{k \in \mathbb{Z} }^{} \left\langle x, g_k \right\rangle g_k\left( t \right).$ where 
                              \[\begin{aligned}
                                                        g_k\left( t \right) &=2B \text{sinc} \left( 2B\left( t - kT \right) \right), \text{ } k \in \mathbb{Z} \\
                      \hat{g}_k\left( f \right) &= 
                      \begin{cases}
                          e^{-i2\pi kfT} &\text{ if } \left| f \right| \le B,\\
                          0 &\text{ otherwise.} 
                      \end{cases}
                              \end{aligned}\]

                              This frame is tight with frame bound $A = 1/T$. Furthermore, the frame is exact if the sampling rate satisfies $1/T = 2B$.
\end{sectionbox}

\begin{sectionbox}[Integral representation for DFT product]
            Let $\left\{ a_{k} \right\}_{k \in \mathbb{Z} } , \left\{ b_{k} \right\}_{k \in \mathbb{Z} }  \in l^{2}$ with discrete time Fourier transform $\hat{a}\left( f \right) = \sum_{k = -\infty}^{\infty} a_k e^{-i2\pi kf}$ and $\hat{b}\left( f \right) = \sum_{k = -\infty}^{\infty} b_k e^{-i2\pi kf}$. By Parseval's theorem, we can write
        \[
            \sum_{k = -\infty}^{\infty} a_k b^{*}_k = \int_{-1/2}^{1/2} \hat{a}\left( f \right) \hat{b}^{*}\left( f \right)df
        \]
\end{sectionbox}

\begin{sectionbox}[DFT matrix at critical sampling]
    The elements of the $N \times N$ DFT matrix $F$ are $\left[ F \right]_{k,l} = \frac{1}{\sqrt{N} } e^{i2\pi \frac{kl}{n}} = \frac{1}{\sqrt{N} }\omega _N^{kl}$ where $\omega _N = e^{-i2\pi /N}$.

    This lets us write, in sum form for each spectrum index $k$ or signal time $n$
    \begin{enumeratenosep}
   \item $ \hat{x}\left[ k \right] = \frac{1}{\sqrt{N} } \sum_{n = 0}^{N - 1} x\left[ n \right]\omega _N^{kn}$,
   \item $ x\left[ n \right] = F^{H}\hat{x}=\frac{1}{\sqrt{N}  } \sum_{k = 0}^{N - 1} \hat{x}\left[ k \right] \omega _N^{-kn}$.
    \end{enumeratenosep}
    or in matrix-vector form simply 
    \begin{enumeratenosep}
    \item $\hat{x} = Fx$ where the $k$th element of $\hat{x}$ is $\hat{x}\left[ k \right]$,
    \item $x = F^{H}\hat{x}$ where the $n$th element of $x$ is $x\left[ n \right]$.
    \end{enumeratenosep}

The DFT matrix is unitary. It is a Vandermonde matrix.

\end{sectionbox}

\begin{sectionbox}[DFT matrix at under- or oversampling]
    If sampling is not critical, we consider a $M \times N$ matrix instead of the square DFT matrix. $M$ is the size of the spectrum, $N$ is the number of samples. Also, we scale all entries by $1/\sqrt{M} $ instead of $1/\sqrt{N} $. We have:
    \begin{enumeratenosep}[i.]
        \item Oversampling with $M > N$. We have a tall matrix $F_o$ which contains the first $N$ columns of the $M \times M$ DFT matrix $F$.
        \item Undersampling with $M < N$. We have a wide matrix $F_u$ which contains the first $M$ rows of the $N \times N$ DFT matrix.
    \end{enumeratenosep}
\end{sectionbox}



% skip selectors

\begin{sectionbox}[Plancherel/Parseval theorem for Fourier transforms]
    If a function $f$ is both in $L^{1} \left( \mathbb{R}  \right)$ and in $L^{2} \left( \mathbb{R}  \right)$ (i.e. $f \in L^{1} \left( \mathbb{R}  \right) \cap L^{2} \left( \mathbb{R}  \right)$) and its Fourier transform is $\hat{f}$, then we can write
    \[
       \left\Vert f \right\Vert_2^{2} =  \int_{-\infty}^{\infty} \left| f\left( x \right) \right|^{2}dx = \int_{-\infty}^{\infty} \left| \hat{f}\left( \xi  \right) \right|^{2} d\xi = \left\Vert \hat{f} \right\Vert _2^{2}
    \]
\end{sectionbox}

\begin{sectionbox}[Riemann-Lebesgue Lemma]
If $x \in L^{1} $, then its Fourier transform $\hat{x}$ is uniformly continuous and satisfies $    \left| \hat{x}\left( f \right) \right| \le \int_{-\infty}^{\infty}  \left| x\left( t \right) \right|dt <\infty \text{ for } f \in \mathbb{R} $ and $    \lim_{\left| f \right| \to \infty} \hat{x}\left( f \right) = 0.$
\end{sectionbox}









