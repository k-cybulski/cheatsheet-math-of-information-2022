\section{Frames}\label{sec:frames}

\begin{sectionbox}
    A set of elements $\G = \left\{ g_k \right\}_{k \in \K}$, $g_k \in\mathcal{H} $, $k \in \K$ is a frame for the Hilbert space $\mathcal{H} $ if there exist $A, B \in \R$ with $0 < A \le  B < \infty$ such that
\[
    A\left\Vert x \right\Vert ^{2} \le \sum_{k \in \K}^{} \left| \left\langle x, g_k \right\rangle  \right|^{2} \le B\left\Vert x \right\Vert ^{2}
\] for all $x \in \mathcal{H}$. The greatest $A$ and least $B$ which fulfil the above criteria are called the frame bounds.
\end{sectionbox}

\begin{sectionbox}[Analysis operator $\mathbb{T} $]
    The analysis operator $\mathbb{T} $ assigns to each signal $x \in \mathcal{H}$ the sequence of inner products $\mathbb{T} x = \left\{ \left\langle x, g_k \right\rangle  \right\}_{k \in \K}$.

    The adjoint of $\mathbb{T} $ is \[\mathbb{T} ^{*} : \left\{ c_k \right\}_{k \in \K}\mapsto \sum_{k \in \K}^{} c_kg_k.\]
\end{sectionbox}

\begin{sectionbox}[Frame operator]
    If  $\left\{ g_{k} \right\}_{k \in \K} $ is a frame on $\mathcal{H} $, then its frame operator $\mathbb{S} : \mathcal{H}  \to \mathcal{H} $ is defined using the linear (analysis) operator $\mathbb{T} $ mapping $x$ to a sequence of inner products $\mathbb{T} : x \mapsto \left\{ \left\langle x, g_k \right\rangle  \right\}_{k \in \K}$ as
\[
    \mathbb{S} = \mathbb{T} ^{*}\mathbb{T} 
\] where $\mathbb{S} x$ is given explicitly by
\[
    \mathbb{S} x = \sum_{k \in \K}^{}  \left\langle x, g_k \right\rangle g_k.
\]

With the frame operator, we can write (Proof \ref{proof:frame_operator_sum})
\[
    \sum_{k \in \K}^{} \left| \left\langle x, g_k \right\rangle  \right|^{2} = \left\langle \mathbb{S}  x, x \right\rangle,
\] so the frame condition now looks like
\[
    A\left\Vert x \right\Vert ^{2} \le \left\langle \mathbb{S} x, x \right\rangle  \le B\left\Vert x \right\Vert ^{2}.
\]

It is self-adjoint $\mathbb{S} ^{*} = \mathbb{S} $  (Proof \ref{proof:frame_operator_self_adjoint}).

It is positive definite, i.e. $\left\langle \mathbb{S} x, x \right\rangle > 0$ for all $x \in \mathcal{H}$, $x \neq 0$. 
\end{sectionbox}



\begin{sectionbox}[Moore-Penrose left inverse]
    The Moore-Penrose left-inverse of $\textbf{A} $ is $\textbf{A} ^{\dagger} := \left( \textbf{A} ^{H}\textbf{A}  \right)^{-1}\textbf{A} ^{H}$. This way,       $ \textbf{A}^{\dagger} \textbf{A} = \textbf{I} _M.$ It exists and is unique if $\mathbb{A} ^{H}\mathbb{A} $ is invertible (full rank).

    Every left inverse of $\mathbb{A} $ can be written as $\textbf{L}  = \textbf{A} ^{\dagger} + \textbf{M} \left( \textbf{I} _N - \textbf{A} \textbf{A} ^{\dagger} \right)$ where $\mathbb{M} $ is an arbitrary matrix (of the correct dimensions). (Proof \ref{proof:every_left_inverse_moore_penrose}).

\end{sectionbox}


\begin{sectionbox}[Bessel sequence]
    $\left\{ g_{k} \right\}_{k \in \K} $ is a Bessel sequence if $g_k$ are functions in a Hilbert space $\mathcal{H} $ and for any $x \in \mathcal{H}$
$    \sum_{k \in \K}^{} \left| \left\langle x, g_k \right\rangle  \right|^{2} < \infty.$

If $\mathbb{T} $ is an operator mapping $\mathcal{X} $ to the space of sequences of inner products with a Bessel sequence $\left\{ g_k \right\}_{k \in \mathcal{K} }$, then
\[
    \left\Vert \mathbb{T} x \right\Vert ^{2} = \sum_{k \in \K}^{}  \left| \left\langle x, g_k \right\rangle  \right|^{2}.
\]
\end{sectionbox}


% SKIP analysis matrices

\subsection{Duals}
\begin{sectionbox}[Dual of a matrix]
    Consider a set of $N$ vectors $\G = \left\{ \textbf{g} _1,  \dots  ,\textbf{g} _N  \right\}$, $\textbf{g} _k \in \Co^{M}$ with $N \ge M$ that spans $\Co^{M}$. 
            The dual of $\G$ is the set of vectors $\left\{ \tilde{\textbf{g}}_1,  \dots  ,\tilde{\textbf{g}} _N  \right\}$ such that for any $x \in \Co^{M}$, $           \textbf{x}  = \sum_{k = 1}^{N} \left\langle \textbf{x} , \textbf{g} _k \right\rangle \tilde{\textbf{g} }_k. $
We can find it by computing a left-inverse $\textbf{L} $ to the analysis matrix.

The \emph{canonical dual} corresponds to the Moore-Penrose left-inverse $\textbf{T} ^{\dagger} = \left( \textbf{T} ^{H}\textbf{T}  \right)^{-1}\textbf{T} ^{H}$.
\end{sectionbox}


\begin{sectionbox}[Canonical dual frame operator]
    Let $\left\{ g_{k} \right\}_{k \in \K} $ be a frame for the Hilbert space $\mathcal{H}$ with frame bounds $A$ and $B$, and let $\mathbb{S} $ be its corresponding frame operator. The set $\left\{ \tilde{g}_{k} \right\}_{k \in \K} $ given by
\[
    \tilde{g}_k = \mathbb{S} ^{-1}g_k, \text{ $k \in \K$} 
\] is a frame for $\mathcal{H}$ with tightest frame bounds $\tilde{A} = 1/B$ and $\tilde{B} = 1/A$.  (Proof \ref{proof:inverse_frame_operator})

We define the canonical dual operator by $   \tilde{\mathbb{S} } := \sum_{k \in K}^{} \left\langle x, \tilde{g}_k \right\rangle \tilde{g}_k$ where $\tilde{g}_k = \mathbb{S} ^{-1}g_k$. It satisfies $\tilde{\mathbb{S} } = \mathbb{S} ^{-1}$.

\end{sectionbox}

\begin{sectionbox}[Decompositions using a frame and its dual]
The analysis operator $\tilde{\mathbb{T} }$ of $\mathbb{S} ^{-1}$ satisfies  $ \tilde{\mathbb{T} }x = \mathbb{T} \left( \mathbb{T} ^{*}\mathbb{T}  \right)^{-1}x$.  (Proof \ref{proof:dual_analysis_operator})
    
Every signal $x \in \mathcal{H} $ can be decomposed as
\begin{enumerate}[1.]
\item $x = \mathbb{T} ^{*}\tilde{\mathbb{T} }x = \sum_{k \in \K}^{} \left\langle x, \tilde{g}_k \right\rangle g_k$,
\item $x = \tilde{\mathbb{T} }  ^{*}\mathbb{T} x = \sum_{k \in \K}^{} \left\langle x, g_k \right\rangle \tilde{g}_k$.
\end{enumerate}
$\mathbb{T} $ and $\tilde{\mathbb{T} }$ are related by
\[
    \mathbb{T} ^{*}\tilde{\mathbb{T} } = \tilde{\mathbb{T} }^{*}\mathbb{T}  = \mathbb{I} _\mathcal{H} .
\]
\end{sectionbox}

\begin{sectionbox}[Biorthogonality between a frame and its dual]
    To check for biorthogonality of $\left\{ g_{k} \right\}_{k \in \mathcal{K} } $ and $\left\{ \tilde{g}_{k} \right\}_{k \in \mathcal{K} } $, we need to see if for each $m \in \mathcal{K} $ it is true that
\[
    \frac{1 - \left| \left\langle g_m, \tilde{g}_m \right\rangle  \right|^{2} - \left| 1 - \left\langle g_m, \tilde{g}_m \right\rangle  \right|^{2}}{2} = 0.
\]

By Lemma 1.34 we know that for each $m \in \mathcal{K} $
\[
    \sum_{k \neq m}^{} \left| \left\langle g_m, \tilde{g}_k \right\rangle  \right|^{2} = \frac{1 - \left| \left\langle g_m, \tilde{g}_m \right\rangle  \right|^{2} - \left| 1 - \left\langle g_m, \tilde{g}_m \right\rangle  \right|^{2}}{2}.
\] If the right hand side is 0, then so will the sum on the left which defines biorthogonality.
\end{sectionbox}


\subsection{Tight frames}
\begin{sectionbox}[Tight frame]
    The frame $\left\{ g_{k} \right\}_{k \in \K} $ is tight if its bounds $A$ and $B$ are equal.

    The frame operator $\mathbb{S} $ corresponds to a tight frame if and only if $\mathbb{S}  = A \mathbb{I} _\mathcal{H} $ where $A$ is some constant. (Proof \ref{proof:tight_frame_scaling} and of converse \ref{proof:tight_frame_scaling_converse})
\end{sectionbox}


\begin{sectionbox}[Square roots of frame operators construct tight frames]
We can use any frame for $\mathcal{H} $ to construct a tight frame with bound $A = 1$ as $\left\{ \mathbb{S} ^{-1/2}g_{k} \right\}_{k \in \K} $  (Proof \ref{proof:square_root_frame_operator}).
\end{sectionbox}

\begin{sectionbox}[Tight frames with bound of 1 are an orthonormal basis]
    If a frame $\left\{ g_{k} \right\}_{k \in \K} $ for the Hilbert space $\mathcal{H}$ is tight with $A = 1$ and $\left\Vert g_k \right\Vert  = 1$ for all $k \in \K$, then it's an orthonormal basis for $\mathcal{H}$. (Proof \ref{proof:tight_frames_onb})

    Every orthonormal basis is a tight frame, but a tight frame is an orthonormal basis only if it fulfils the above criteria.
\end{sectionbox}

\begin{sectionbox}[Theorem of Naimark]
    Let $N > M$. Suppose that the set $\left\{ \textbf{g}_1, \dots, \textbf{g} _N \right\}$, $\textbf{g} _k \in \mathcal{H}, k = 1, \dots, N$ is a tight frame for an $M$-dimensional Hilbert space $\mathcal{H}$ with frame bound $A = 1$. Then, there exists an $N$-dimensional Hilbert space $\K \supset \mathcal{H}$ and an ONB $\left\{ \textbf{e} _1, \dots, \textbf{e} _N \right\}$ for $\K$ such that $\P \textbf{e} _k = \textbf{g} _k$, $k = 1, \dots, N$, where $\P : \K \to \K$ is the orthogonal projection onto $\mathcal{H}$.
\end{sectionbox}

\begin{sectionbox}[Exact and inexact frames]
    Let $\left\{ g_{k} \right\}_{k \in \K} $ be a frame for $\mathcal{H}$. 
        \begin{enumerate}[1.]
            \item 
We call the frame $\left\{ g_{k} \right\}_{k \in \K} $ exact if for all $m \in \K$, the set $\left\{ g_{k} \right\}_{k \neq m} $ is incomplete for $\mathcal{H}$.
\item We call the frame $\left\{ g_{k} \right\}_{k \in \K} $ inexact if there is at least one element $g_m$ that can be removed from the frame so that the set $\left\{ g_{k} \right\}_{k \neq m} $ is again a frame.
        \end{enumerate}

        Alternatively, we can specify exact and inexact frames by biorthogonality of their duals (Proof \ref{proof:exact_frames_biorthogonality})
        \begin{enumerate}[1.]
    \item $\left\{ g_{k} \right\}_{k \in \K} $ is exact if and only if $\left\langle g_m, \tilde{g}_m \right\rangle  = 1$ for all $m \in \K$,
    \item $\left\{ g_{k} \right\}_{k \in \K} $ is inexact if and only if there exists at least one $m \in \K$ such that $\left\langle g_m, \tilde{g}_m \right\rangle \neq 1.$
\end{enumerate}

\end{sectionbox}

