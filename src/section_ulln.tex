\section{Uniform Laws of Large Numbers}\label{sec:ulln}

\begin{notebox}[Notation]
    Let $\mathcal{F} $ be a class of functions.
    \begin{enumeratenosep}[i.]
        \item  If $X_i$ are i.i.d. samples from a distribution $P$, we write $\left\Vert \hat{P}_n - P  \right\Vert _\mathcal{F}  = \sup _{f \in \mathcal{F} } \left| \frac{1}{n} \sum_{i = 1}^{n}  f\left( X_i \right) - \Exnb \left[ f\left( X \right) \right]  \right|$.
        \item If $x_1^{n} = \left( x_1, \dots, x_n \right)$ is a point set, then we define $\mathcal{F} \left( x_1^{n} \right)$ as the set of $n$-dimensional vectors that can be "reached" by a function class $\mathcal{F} \left( x_1^{n}  \right) = \left\{ f\left( x_1 \right), f\left( x_2 \right), \dots, f\left( x_n \right) : f \in \mathcal{F}  \right\}.$
    \end{enumeratenosep}
\end{notebox}


\begin{sectionbox}[Convergence - almost sure and in probability]
    The sequence $X_1, \dots$ converges in probability to $X$ if
\[
    \lim_{n \to \infty} \mathbb{P} \left( \left| X - X_n \right| > \varepsilon  \right) = 0 \text{ for all } \varepsilon  > 0.
\]

It converges almost surely to $X$ if
\[
    \mathbb{P} \left( \lim_{n \to \infty} X_n = X \right) = 1.
\]

Almost sure convergence implies convergence in probability.
\end{sectionbox}

\begin{sectionbox}[Sup-norm distance]
    Let $F, G$ be functions from $\mathcal{X} $ to $\mathbb{R} $. The sup-norm of $F : \mathcal{X}  \to \mathbb{R} $ is defined as $ \left\Vert F \right\Vert _\infty = \sup _{t \in \mathcal{X}  }\left| G\left( t \right) - F\left( t \right) \right|.$
\end{sectionbox}

\begin{sectionbox}[Continuity in the sup-norm]
    We say that a functional $\gamma $ is continuous at $F$ in the sup-norm if for all $\varepsilon  > 0$, there exists a $\delta  > 0$ such that if $\left\Vert G - F \right\Vert _\infty \le \delta $ then $\left| \gamma \left( G \right) - \gamma \left( F \right) \right| \le  \varepsilon $.
\end{sectionbox}

\begin{sectionbox}[Continuity in the sup-norm implies empirical functional converges to true value]
    Let $F$ be the distribution function of a probability distribution $\mathbb{P} $, and $\hat{F}_n $ be its corresponding empirical distribution function.         Show that if a functional $\gamma : \mathcal{F}  \to \mathbb{R} $ is continuous in the sup-norm at $F$ then $\gamma \left( \hat{F}_n  \right)$ converges in probability to $\gamma \left( F  \right)$:     $\gamma \left( \hat{F}_n  \right) \xrightarrow[n \to \infty]{\mathbb{P} }  \gamma \left( F \right).$ (Proof \ref{proof:sup_norm_convergence})
\end{sectionbox}

\begin{sectionbox}[Glivenko-Cantelli classes]
    We say that $\mathcal{F} $ is a Glivenko-Cantelli class for a probability measure $P$ if $    \left\Vert \hat{P}_n - P  \right\Vert _\mathcal{F} = \sup _{f \in \mathcal{F} } \left| \frac{1}{n} \sum_{i = 1}^{n}  f\left( X_i \right) - \Exnb \left[ f\left( X \right) \right]  \right| . \xrightarrow[n \to \infty]{\mathbb{P} } 0.$
\end{sectionbox}

\begin{sectionbox}[Empirical risk and population risk in empirical risk minimization]
    A standard approach for estimating $\theta ^{*}$ from our $\left\{ X_i \right\}_{i = 1}^{n}$ is based on minimizing a cost function $\mathcal{L} _\theta \left( X \right)$ which quantifies the fit between $\theta \in \Omega $ and the sample $\left\{ X_i \right\}_{i = 1}^{n}$.
\begin{enumeratenosep}[1.]
    \item 
          The empirical risk is
             $ \hat{R}_n \left( \theta , \theta ^{*} \right) = \frac{1}{n}\sum_{i = 1}^{n} \mathcal{L} _\theta \left( X_i \right)$
      \item 
          The population risk is
             $ R\left( \theta , \theta ^{*} \right) = \Exnb _{\theta ^{*}}\left[ \mathcal{L} _\theta \left( X \right) \right].$
\end{enumeratenosep}
\end{sectionbox}

\begin{sectionbox}[Excess risk in empirical risk minimization]
    We typically minimize the risk over a subset $\Omega _0$ of $\Omega $ to produce an estimator $\hat{\theta }$. Excess risk is then the difference between risk of $\hat{\theta }$ and of the best estimator in $\Omega _0$
\[
    E\left( \hat{\theta }, \theta  \right) = R\left( \hat{\theta }, \theta ^{*}  \right) - \inf _{\theta  \in \Omega _0} R\left( \theta , \theta ^{*} \right).
\]

Excess risk can be bounded by  $2\left\Vert \hat{P}_n  - P \right\Vert _\mathcal{F}$ (Proof \ref{proof:bounding_excess_risk_empirical_process}), which means that if the class of loss functions is Glivenko-Cantelli, then the empirical risk will converge to the theoretical risk as $n \to \infty$.
\end{sectionbox}

\begin{sectionbox}[Rademacher Complexity]
    We define the empirical Rademacher complexity of $\mathcal{F} $ as
\[
    \mathcal{R} \left( \mathcal{F} \left( x_1^{n} \right)/n \right) = \Exnb _\varepsilon \left[ \sup _{f \in \mathcal{F} } \left| \frac{1}{n} \sum_{i = 1}^{n} \varepsilon _i f\left( x_i \right) \right| \right]
\] where $\left\{ \varepsilon _k \right\}_{k = 1}^{n}$ is an i.i.d. sequence of Rademacher random variables, taking values in $\left\{ -1, 1 \right\}$ with equal probability.
\end{sectionbox}

\begin{sectionbox}
The (theoretical) Rademacher complexity is then
\[\begin{aligned}
    \mathcal{R}_n\left(\mathcal{F}\right) &= \Exnb _X \left[ \mathcal{R}\left(\mathcal{F}\left( X_1^n \right)/n  \right)  \right]\\
                                                                &= \Exnb _{\varepsilon , X} \left[ \sup _{f \in \mathcal{F} } \left| \frac{1}{n} \sum_{i = 1}^{n}  \varepsilon _i f\left( X_i \right) \right| \right].
\end{aligned}\]
\end{sectionbox}




\begin{sectionbox}[Showing ULLN by Rademacher complexity]
    Theorem 12.10: For any $b$-uniformly bounded function class, i.e. $    \left\Vert f \right\Vert _\infty     \le b, \text{ for all }  f \in \mathcal{F} ,$ any positive integer $n \ge 1$, and any $\delta  \ge 0$, we have $   \left\Vert \hat{P}_n - P  \right\Vert _\mathcal{F}  \le 2 \mathcal{R} _n\left( \mathcal{F}  \right) + \delta $ with probability $1 \ge e^{-\frac{n\delta ^{2}}{2b^{2}}}$. Hence, as long as $\mathcal{R} _n \left( \mathcal{F}  \right) = o\left( 1 \right)$, we have $\left\Vert \hat{P}_n - P  \right\Vert _\mathcal{F}  \xrightarrow[n \to \infty]{\mathbb{P}  } 0$.
\end{sectionbox}


\begin{sectionbox}[Lower-bounding sup-norm of empirical vs true function value]
Proposition 12.11.: For every $b$-uniformly bounded function class $\mathcal{F} $, any integer $n \ge 1$, and any $\delta  \ge 0$, we have\[
    \left\Vert \hat{P}_n - P  \right\Vert _\mathcal{F}  \ge \frac{1}{2} \mathcal{R} _n \left( \mathcal{F}  \right) - \frac{\sup _{f \in \mathcal{F} } \left| \Exnb _\mathbb{P} \left( f \right) \right|}{2 \sqrt{n} } - \delta 
\] with probability $ \ge 1 - e^{-\frac{n\delta ^{2}}{2b^{2}}}$.

\end{sectionbox}

\begin{sectionbox}[Polynomial discrimination]
    A class of functions $\mathcal{F} $ with domain $\mathcal{X} $ has polynomial discrimination of order $\nu \ge 1$ if for each $n \in \mathbb{N} $ and collection $x_1^{n}=\left( x_1, \dots, x_n  \right)$ of $n$ points in $\mathcal{X} $, the set $\mathcal{F} \left( x_1^{n} \right)$ satisfies a polynomial upper bound on its cardinality
\[
    \left| \mathcal{F} \left( x_1^{n} \right) \right| \le \left( n + 1 \right)^{N}.
\]
\end{sectionbox}

\begin{sectionbox}[Sub-gaussian random variables]
   $X$ is a sub-Gaussian random variable if its characteristic function satisfies $                       \Exnb \left[ e^{\lambda \left( X - \mu   \right)} \right] \le e^{\frac{\sigma ^{2}\lambda ^{2}}{2}}$ for all $\lambda  \in \mathbb{R} $.
\end{sectionbox}

\begin{sectionbox}[Empirical Rademacher complexity is sub-gaussian]
    For a fixed set of points $x_1^{n}$, the empirical Rademacher complexity $\mathcal{R}_n \left( \mathcal{F} \left( x_1^{n} \right)/n \right)$ is sub-Gaussian.  (Proof \ref{proof:rademacher_complexity_sub_gaussian})
\end{sectionbox}

\begin{sectionbox}[Upper bound on Rademacher complexity by polynomial discrimination]
    Suppose that $\mathcal{F} $ has polynomial discrimination of order $\nu$. Then, for all $n \in \mathbb{N} $ and any collection of points $x_1^{n} = \left( x_1, \dots, x_n  \right)$ we have an upper bound
\[
   \mathcal{R}\left(\mathcal{F}\left( x_1^n \right)/n  \right) = \Exnb _\varepsilon \left[ \sup _{f \in \mathcal{F} } \left| \frac{1}{n}\sum_{i = 1}^{n}  \varepsilon _i f\left( x_i \right) \right| \right] \le 4D\left( x_1^{n} \right) \sqrt{\frac{\nu \log \left( n + 1 \right)}{n}} ,
    \] where \[D\left( x_1^{n} \right) = \sup _{f \in \mathcal{F} } \sqrt{\frac{\sum_{i = 1}^{n}  f^{2}\left( x_i \right)}{n}} \] is the $l^{2}$-radius of the set $\mathcal{F} \left( x_1^{n} \right)/\sqrt{n} $.
\end{sectionbox}



\begin{sectionbox}[Classical Glivenko-Cantelli theorem]
    Let $F\left( t \right) = P\left[ X \le t \right]$ be the CDF of a random variable $X \sim \mathbb{P} $, and let $\hat{F}_n $ be the empirical CDF bsed on $n$ i.i.d. samples $X_i \sim \mathbb{P} $. Then we have that
\[
    \mathbb{P} \left[ \left\Vert \hat{F}_n - F  \right\Vert _\infty \ge 8 \sqrt{\frac{\log \left( n + 1 \right)}{n}}  + \delta  \right] \le e^{-\frac{n\delta ^{2}}{2}}
\] for all $ \delta  \ge 0$, so $\left\Vert \hat{F}_n - F  \right\Vert _\infty \xrightarrow[n \to \infty]{\mathbb{P} } 0$. (Proof \ref{proof:classical_glivenko_cantelli})
\end{sectionbox}

\subsection{Vapnik-Chervonenkis dimension}

\begin{sectionbox}[Shattering sets]
    Given a class $\mathcal{F} $ of binary-valued functions, we say that the set $x_1^{n} = \left( x_1, \dots, x_n \right)$ is shattered by $\mathcal{F} $ if $\left| \mathcal{F} \left( x_1^{n} \right) \right| = 2^{n}$.
\end{sectionbox}

\begin{sectionbox}[Vapnik-Chervonenkis dimension and classes]
    The Vapnik-Chervonenkis dimension $\nu\left( \mathcal{F}  \right)$ is the largest integer $n$ for which there is a collection $x_1^{n} = \left( x_1, \dots, x_n  \right)$ of $n$ points that is shattered by $\mathcal{F} $.

When $\nu\left( \mathcal{F}  \right)$ is finite, $\mathcal{F} $ is said to be a Vapnik-Chervonenkis class.

\end{sectionbox}

\begin{sectionbox}[Example of VC dimension - intervals on $\mathbb{R} $]
    Define $\mathcal{S} _{\text{two}} = \left\{ \left[ a, b \right] : a,b \in \mathbb{R} \text{ such that }a < b  \right\}$. Let $\mathcal{F} $ be the set of indicator functions of $\mathcal{S} _{\text{two}} $. VC dimension of $\mathcal{F} $ is 2. (Proof \ref{proof:vc_dimension_of_intervals})
\end{sectionbox}


\begin{sectionbox}[Vapnik-Chervonenkis/Sauer-Shelah theorem]
    Consider a set class $\mathcal{S} $ with $\nu\left( \mathcal{S}  \right)< \infty$. Then, for any collection of points $\mathcal{P}  = \left( x_1, \dots, x_n  \right)$ with $n \ge \nu\left( \mathcal{S}  \right)$, we have
\[
    \left| \mathcal{S} \left( \mathcal{P}  \right) \right| \le \sum_{i = 0}^{\nu\left( \mathcal{S}  \right)}  \binom{n}{i} \le \left( n + 1 \right)^{\nu \left( \mathcal{S}  \right)}
\]
\end{sectionbox}

